<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <base href="<?php echo $s['siteRoot'];?>">
    <link rel="stylesheet" type="text/css" href="styles/reset.css">
    <link rel="stylesheet" type="text/css" href="styles/jg.css">
    <link rel="stylesheet" type="text/css" href="styles/main.css">
    <script  src="js/jquery.js"></script>
    <script  src="js/organ.js"></script>
    
    <title>辅导机构</title>
</head>
<body>
    <!-- top开始 -->
    <?php import_part("Custom.part","header"); ?>
<!-- erji_daohang开始 -->
<!-- 显示用户浏览位置开始 -->
<div class="bread_nav">
    <a href="<?php e_page("home","index");?>">首页</a><span>&gt;&gt;</span>辅导机构
</div>
<!-- 显示所有机构的页面开始 -->
<?php foreach($r as $key=>$v) {?>
<div class="jg">
<!-- 语言类的开始写 -->
    <div id="item<?php echo isset($v[0]['classify_id'])?$v[0]['classify_id']:'' ?>">
        <h3><?php echo $key ?></h3>
        <!-- 每个机构小方框开始写了 -->   
        <?php foreach($v as $value){?>
            <div class="list_jigou">
                <a href="<?php $a=$value['organization_id']; e_page("organization","organ_course","id=$a"); ?>"><img src="<?php echo $value['image_url'] ?>"></a>
                <h4><a href="<?php $a=$value['organization_id']; e_page("organization","organ_course","id=$a"); ?>"><?php echo $value['name']?></a></h4>
                <p>地址：<?php echo $value['address']?></p>
            </div>
        <?php }?>
        <!-- 显示更多机构开始写 -->
        <!-- <div class="list_more">
            <p><a href="#">点击即可查询更多机构</a></p>
        </div> -->
    </div>
</div>
<?php }?>
<!-- 漂浮导航开始写 -->
<div class="float_nav">
    <ul>
        <li  onclick="scroll(1)">语言</li>
        <li  onclick="scroll(2)">资格证</li>
        <li  onclick="scroll(4)">考研</li>
        <li  onclick="scroll(5)">公务员</li>
        <li  onclick="scroll(6)">艺能</li>
    </ul>
</div>
<!-- footer开始 -->
<?php import_part("Custom.part","footer"); ?>
</body>
</html>