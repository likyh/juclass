
<table id="dataTable">
<thead>
<tr>
    <th>报名课程</th>
    <th>手机号</th>
    <th>报名时间</th>
    <th>订单状态</th>
    <th>管理</th>
</tr>
</thead>
<tbody>
<?php foreach ($result['list'] as $v){ ?>
<tr>
    <td><?php echo $v['course_name']?></td>
    <td><?php echo $v['phone']?></td>
    <td><?php echo $v['date']?></td>
    <td><?php  echo $v['offorder_progress'] ?></td>
    <td><a href="<?php e_page("home","offorder_control",array('id'=> $v['id']))?>">修改</a> <a onclick="return confirm('你真的要删除吗？') ? true : false"  href="<?php e_page("home","offorder_delete",array('id'=> $v['id']))?>">删除</a></td>
</tr>
<?php }?>
</tbody>
</table>
<div id="dataPage">
<ul>
    <li>共<span><?php echo (int)$result['pageNum']; ?></span>页/<span><?php echo $result['total']; ?></span>条记录</li>
    <?php if(($result['page']-1)>0){?>
    <li ><a href="<?php e_page("home","order",array('page_id'=>$result['page']-1)) ?>">上一页</a></li>
    <?php }?>
        <?php for($i=1;$i<=$result['pageNum'];$i++){?>
        <li class="page"><a href="<?php e_page("home","order",array('page_id'=>$i)) ?>"><?php if($result['page']==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></li>
        <?php } ?>
        <?php if(($result['page']+1)<$result['pageNum']){?>
    <li ><a href="<?php e_page("home","order",array('page_id'=>$result['page']+1)) ?>">下一页</a></li>
    <?php } ?>
</ul>
</div>