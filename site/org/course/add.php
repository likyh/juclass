<form  method="post" action="<?php e_page("home","courseAdd_after")?>"  enctype="multipart/form-data">
    <fieldset>
        <legend>课程信息(请完整填写)</legend>
        <label for="course_num">课程编号</label> 
        <input type="text" name="course_num" id="course_num"   placeholder="请输入课程编号,编号越大前端显示顺序越靠前"/>
        <br/>
        <label for="course_name">课程名称</label>
        <input type="text" name="course_name" id="course_name"   placeholder="请输入课程名称"/>
         <br/>
        <label for="current_price">聚课价</label>
        <input type="text" name="current_price" id="current_price"   placeholder="请输入课程聚课价格"/>
         <br/>
        <label for="original_price">原价</label>
        <input type="text" name="original_price" id="original_price"   placeholder="请输入课程原价"/>
        <br/>
        <label for="progress">课程进度</label>
        <input type="text" name="progress" id="progress"  placeholder="请输入课程进度"/>
        <br/>
        <label for="start_time">课程开始时间</label>
        <input type="text" name="start_time" id="start_time"  placeholder="请输入课程开始时间"/>
        <br/>
        <label for="end_time">课程结束时间</label>
        <input type="text" name="end_time" id="end_time"  placeholder="请输入课程结束时间"/>
        <br/>
        <label for="lession_time">上课时间安排</label>
        <input type="text" name="lession_time" id="lession_time"  placeholder="请输入课程时间安排"/>
        <br/>
        <label for="class_hour">课时</label>
        <input type="text" name="class_hour" id="class_hour"  placeholder="请输入课时"/>
        <br/>
        <label for="course_adress">上课地点</label>
        <input type="text" name="course_adress" id="course_adress"  placeholder="请输入上课地点"/>
    </fieldset>
    <fieldset>
        <legend>课程相关资料上传</legend>
        <label for="video_url">试听视频上传</label>
        <input type="file" name="video_url" id="video_url" /> 
        <label for="image_url">课程图片上传(420px*270px)</label>
        <input type="file" name="image_url" id="image_url" /> 
        <label for="schedule_url">课表图片上传</label>
        <input type="file" name="schedule_url" id="schedule_url" />        
    </fieldset>
    <fieldset>
        <legend>* 请选择该课程老师</legend>
        <?php foreach($result['teacher'] as $v){?>
        <label for="<?php echo $v['id']?>"><?php echo $v['teacher_name']?></label>
        <input type="radio" name="teacher_id" id="<?php echo $v['id']?>" value="<?php echo $v['id']?>" />
        <?php } ?>
    </fieldset>
    <fieldset>
        <legend>* 请选择课程分类</legend>
        <?php foreach ($result['classify'] as $value1) {?>
            <legend>[<?php echo $value1['name'] ?>]</legend>
            <?php foreach ($value1['subclassify'] as $value2) {?>
                <label for="<?php echo $value2['id'] ?>"><?php echo $value2['name'] ?></label>
                <input type="radio" name="subclassify_id" id="<?php echo $value2['id'] ?>" value="<?php echo $value2['id'] ?>" />

           <?php } ?>
        <?php } ?>
    </fieldset>

    <input type="submit" name="send" />
</form>