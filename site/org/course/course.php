<table id="dataTable">
<thead>
<tr>
    <th>课程编号</th>
    <th>授课名称</th>
    <th>课时</th>
    <th>原价</th>
    <th>聚课价</th>
    <th>授课老师</th>
    <th>操作</th>
</tr>
</thead>
<tbody>
<?php  foreach ($result['list'] as $v){ ?>
<tr>
    <td><?php echo $v['course_num']?></td>
    <td><?php echo $v['course_name']?></td>
    <td><?php echo $v['lession_time']?></td>
    <td><?php echo $v['original_price']?></td>
    <td><?php echo $v['current_price']?></td>
    <td><?php echo $v['teacher_name']?></td>
    <td><a href="<?php e_page("home","course_control",array('id'=> $v['course_id']))?>">修改</a> <a onclick="return confirm('你真的要删除吗？') ? true : false"  href="<?php e_page("home","course_delete",array('id'=> $v['course_id']))?>">删除</a></td>

</tr>
<?php }?>
</tbody>
</table>
<div id="dataPage">
<ul>
    <li>共<span><?php echo (int)$result['pageNum']; ?></span>页/<span><?php echo $result['total']; ?></span>条记录</li>
    <?php if(($result['page']-1)>0){?>
    <li ><a href="<?php e_page("home","course",array('page_id'=>$result['page']-1)) ?>">上一页</a></li>
    <?php }?>
        <?php for($i=1;$i<=$result['pageNum'];$i++){?>
        <li class="page"><a href="<?php e_page("home","course",array('page_id'=>$i)) ?>"><?php if($result['page']==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></li>
        <?php } ?>
        <?php if(($result['page']+1)<$result['pageNum']){?>
    <li ><a href="<?php e_page("home","course",array('page_id'=>$result['page']+1)) ?>">下一页</a></li>
    <?php } ?>
</ul>
</div>