<table id="dataTable">
    <thead>
    <tr>
        <th>id</th>
        <th>资料名称</th>
        <th>路径</th>
        <th>所属课程</th>
        <th>操作</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>id</th>
        <th>资料名称</th>
        <th>路径</th>
        <th>所属课程</th>
        <th>操作</th>
    </tr>
    </tfoot>
    <tbody>
    <?php foreach($result['list'] as $v){?>
    <tr>
        <td><?php echo $v['id']?></td>
        <td><?php echo $v['data_name']?></td>
        <td><?php echo $v['data_url']?></td>
        <td><?php echo $v['course_name']?></td>
        <td> <a onclick="return confirm('你真的要删除吗？') ? true : false"  href="<?php e_page("home","data_delete",array('id'=> $v['id']))?>">删除</a></td>
    </tr>
    <?php }?>
    </tbody>
</table>
<div id="dataPage">
    <ul>
    <li>共<span><?php echo (int)$result['pageNum']; ?></span>页/<span><?php echo $result['total']; ?></span>条记录</li>
    <?php if(($result['page']-1)>0){?>
    <li ><a href="<?php e_page("home","data",array('page_id'=>$result['page']-1)) ?>">上一页</a></li>
    <?php }?>
        <?php for($i=1;$i<=$result['pageNum'];$i++){?>
        <li class="page"><a href="<?php e_page("home","data",array('page_id'=>$i)) ?>"><?php if($result['page']==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></li>
        <?php } ?>
        <?php if(($result['page']+1)<$result['pageNum']){?>
    <li ><a href="<?php e_page("home","data",array('page_id'=>$result['page']+1)) ?>">下一页</a></li>
    <?php } ?>
</ul>
</div>