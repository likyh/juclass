<table id="dataTable">
    <thead>
    <tr>
        <th>id</th>
        <th>评价用户</th>
        <th>评价课程</th>
        <th>评价内容</th>
        <th>评价时间</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($result['list'] as $v){?>
    <tr>
        <td><?php echo $v['id']?></td>
        <td><?php echo $v['user_name']?></td>
        <td><?php echo $v['course_name']?></td>
        <td><?php echo $v['content']?></td>
        <td><?php echo $v['comment_time']?></td>
    </tr>
    <?php }?>
    </tbody>
</table>
<div id="dataPage">
    <ul>
    <li>共<span><?php echo (int)$result['pageNum']; ?></span>页/<span><?php echo $result['total']; ?></span>条记录</li>
    <?php if(($result['page']-1)>0){?>
    <li ><a href="<?php e_page("home","comment",array('page_id'=>$result['page']-1)) ?>">上一页</a></li>
    <?php }?>
        <?php for($i=1;$i<=$result['pageNum'];$i++){?>
        <li class="page"><a href="<?php e_page("home","comment",array('page_id'=>$i)) ?>"><?php if($result['page']==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></li>
        <?php } ?>
        <?php if(($result['page']+1)<$result['pageNum']){?>
    <li ><a href="<?php e_page("home","comment",array('page_id'=>$result['page']+1)) ?>">下一页</a></li>
    <?php } ?>
</ul>
</div>