<form  method="post" action="<?php e_page("home","teacher_change")?>"  enctype="multipart/form-data">
    <fieldset>
        <legend>教师信息</legend>
        <input type="hidden" value="<?php echo $result['list']['id']?>" name="id">  
        <label for="name">教师姓名</label>
        <input type="text" name="teacher_name" id="name" value="<?php echo $result['list']['teacher_name']?>"  placeholder="请填写教师姓名"/>
        <br/>
        <label for="teach_year">教龄</label>
        <input type="text" name="teach_year" id="teach_year" value="<?php echo $result['list']['teach_year']?>"  placeholder="请填写教学年龄"/>
         <br/>
        <label for="email">Email</label>
        <input type="text" name="email" id="email" value="<?php echo $result['list']['email']?>"  placeholder="请填写教师Email"/>
        <br/>
        <label for="phone">联系方式</label>
        <input type="text" name="phone" id="phone" value="<?php echo $result['list']['phone']?>" placeholder="请填写教师联系方式"/>
        <br/>
        <label for="characteristc">教师特色</label>
        <input type="text" name="characteristc" id="characteristc" value="<?php echo $result['list']['characteristc']?>" placeholder="请填写教师特色"/>
        <br/>
    </fieldset>
    <fieldset>
        <label for="teacher_img">教师图片(220px*300px)</label>
        <?php if(!empty($result['list']['teacher_img'])) echo "已上传，重复上传将替换";else echo "未上传"; ?>
        <input type="file" name="teacher_img" id="teacher_img" value="123"/>
    </fieldset>
    <fieldset>
        <label for="intro">教师介绍</label>
        <textarea id="intro" name="intro" placeholder="请填写教师介绍"><?php echo $result['list']['intro']?></textarea>
    </fieldset>
    <input type="submit" name="send" />
</form>