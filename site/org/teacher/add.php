<form  method="post" action="<?php e_page("home","teacherAdd_after")?>"  enctype="multipart/form-data">
    <fieldset>
        <legend>教师信息</legend> 
        <label for="teacher_name">教师姓名</label>
        <input type="text" name="teacher_name" id="teacher_name"   placeholder="请填写教师姓名"/>
        <br/>
        <label for="teach_year">教龄</label>
        <input type="text" name="teach_year" id="teach_year"   placeholder="请填写教学年龄"/>
         <br/>
        <label for="email">Email</label>
        <input type="text" name="email" id="email"   placeholder="请填写教师Email"/>
        <br/>
        <label for="phone">联系方式</label>
        <input type="text" name="phone" id="phone"  placeholder="请填写教师联系方式"/>
        <br/>
        <label for="characteristc">教师特色</label>
        <input type="text" name="characteristc" id="characteristc"  placeholder="请填写教师特色"/>
    </fieldset>
    <fieldset>
        <label for="i5">教师图片(220px*300px)</label>
        <input type="file" name="teacher_img" id="i5" />
    </fieldset>
    <fieldset>
        <label for="intro">教师介绍</label>
        <textarea id="intro" name="intro" placeholder="请填写教师介绍"></textarea>
    </fieldset>
    <input type="submit" name="send" />
</form>