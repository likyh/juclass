<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <base href="<?php echo $s['siteRoot'];?>">
    <link rel="stylesheet" type="text/css" href="styles/reset.css">
    <link rel="stylesheet" type="text/css" href="styles/jgxx.css">
    <link rel="stylesheet" type="text/css" href="styles/main.css">
    <title>机构信息</title>
</head>
<body>
    <!-- top开始 -->
    <?php import_part("Custom.part","header"); ?>
<!--nav开始 -->
<!-- 显示用户浏览位置开始 -->
<div class="bread_nav">
    <a href="<?php e_page("home","index");?>"> 首页</a><span>&gt;&gt;</span>机构中心
</div>
<!-- 显示所有课程的页面开始 -->
<div class="wrap">
<div class="main_course">
    <!-- 开始写单个课程的简介 -->
    <?php foreach($r['course'] as $value){?>
    <div class="list_course">
            <h3><a href="<?php $a=$value['course_id']; e_page("organization","listen","id=$a"); ?>">课程：<?php echo $value['course_name']?></a></h3>
            <p>价格：<span style="font-size:24px; color:#e61c62;font-weight: bold; "><?php echo $value['current_price']?></span>元<span style="margin-left: 30px">原价：<del><?php echo $value['original_price']?></del>元</span></p>
            <p>教师：<?php echo $value['teacher_name']?></p>
            <p class="border">教师特色：<span><?php echo $value['characteristc']?></span></p>
    </div>
    <div class="list_button">
            <input  style="margin-top: 22px" type="image" src="images/stxq.jpg" onclick="window.location.href='<?php $a=$value['course_id']; e_page("organization","listen","id=$a"); ?>'">
            <input  style="margin:22px 0 0 0" type="image" src="images/ljbm.jpg">
    </div>
    <?php }?>
   
</div>
<!-- 开始写右侧机构信息 -->
<div class="jigou_desc">
    <h3><?php  echo $r['organization'][0]['name'];?></h3>
    <img src="<?php  echo $r['organization'][0]['image_url'];?>">
    <p>电话：<?php echo $r['organization'][0]['tele']?></p>
    <p>QQ：<?php echo $r['organization'][0]['qq']?></p>
    <p>主页：<?php echo $r['organization'][0]['webpage']?></p>
    <p>机构特色：<?php echo $r['organization'][0]['characteristic']?></p>
    <p>地址：<?php echo $r['organization'][0]['address']?></p>
    <a href="http://map.baidu.com/?latlng=39.916979519873,116.41004950566&title=<?php  echo $r['organization'][0]['name'];?>&content=<?php echo $r['organization'][0]['address']?>&autoOpen=true&l" target=_blank>查看地图</a>

</div>
<!-- 开始写分页 -->
<div class="page">
    
    <?php if($r['page']>1){?>
        <span><a href="<?php e_page('organization','organ_course',array('page_id'=>$r['page']-1,'id'=>$r['organization'][0]['organization_id']));?>">上一页</a></span>
    <?php }?>
    <?php for($i=1;$i<$r['pageNum'];$i++){?>
        <span><a href="<?php e_page('organization','organ_course',array('page_id'=>$i,'id'=>$r['organization'][0]['organization_id']));?>"><?php if ($r['page']==$i)echo '<strong>'.$i.'</strong>' ; else  echo $i ;?></a></span>
    <?php }?>
    <?php if($r['page']+1<$r['pageNum']){ ?>
        <span><a href="<?php e_page('organization','organ_course',array('page_id'=>$r['page']+1,'id'=>$r['organization'][0]['organization_id']));?>">下一页</a></span>
    <?php }?>
</div>

<!-- footer开始 -->
<?php import_part("Custom.part","footer"); ?>
</body>
</html>
