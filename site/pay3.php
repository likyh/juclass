
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<?php echo $s['siteRoot'];?>">
<link rel="stylesheet" type="text/css" href="styles/reset.css">
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/pay.css">
<script type="text/javascript" src="js/rollpic.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<title>聚课网</title>
</head>

<body>
<!-- top开始 -->
<?php import_part("Custom.part","header"); ?><!-- main开始 -->
<div class="pay_success">
    <img src="images/pay_success.jpg">
    <p class="p1">支付成功!</p>
    <p class="p2">返回<a href="#">个人中心</a>，查看我的课表</p>
</div>


<!-- phone开始 -->
<!-- <div class="phone">
    <div class="phone_content">
        <p>400-234-234<span>（拨打号码可与辅导机构联系）</span></p>
    </div>
</div> -->
<!-- footer开始 -->
<?php import_part("Custom.part","footer"); ?>
</body>
</html>