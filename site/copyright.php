<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<?php echo $s['siteRoot'];?>">
<link rel="stylesheet" type="text/css" href="styles/reset.css">
<link rel="stylesheet" type="text/css" href="styles/main.css">
<title>聚课网主页</title>
</head>

<body>
<!-- top开始 -->
<?php import_part("Custom.part","header"); ?><!-- main开始 -->
<div class="info">
    <h3><?php echo $result['name']?></h3>
    <div class="info_content">
        <?php echo $result['content']?>
    </div>
</div>
<?php import_part("Custom.part","footer"); ?>
</body>
</html>