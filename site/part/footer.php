<div class='footer'>
    <div class="footer_content">
        <ul>
            <li>
                     <h3>机构</h3>
                     <p><a href="http://localhost/juclass/org/home/login">机构登录</a></p>
                     <p><a href="<?php e_page("home","copyright",array('id'=> 1))?>">机构常见问题</a></p>
            </li>
            <li>
                     <h3>网站</h3>
                     <p><a href="<?php e_page("home","copyright",array('id'=> 2))?>">联系我们</a></p>
                     <p><a href="<?php e_page("home","rollnews",array('id'=> 1))?>">推送消息</a></p>
            </li>
            <li>
                     <h3>用户</h3>
                     <p><a href="<?php e_page("home","copyright",array('id'=> 3))?>">用户协议</a></p>
                     <p><a href="<?php e_page("home","copyright",array('id'=> 4))?>">用户须知</a></p>
                     <p><a href="<?php e_page("home","copyright",array('id'=> 5))?>">常见问题</a></p>
            </li>
            <li>
                    <h3>
                        <p>聚课网微信二维码</p>
                        <p><img src="images/erweima.jpg"></p>
                    </h3>
            </li>
        </ul>
    </div>
</div>
<div class="copyright">
    <div class="copyright_content">
        <p>版权所有 徐州空天巨客网络科技有限公司  CopyRight  2014  www.juclass.com</p>
    </div>
</div>