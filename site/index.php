
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<?php echo $s['siteRoot'];?>">
<link rel="stylesheet" type="text/css" href="styles/reset.css">
<link rel="stylesheet" type="text/css" href="styles/main.css">
<script type="text/javascript" src="js/rollpic.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<title>聚课网</title>
</head>

<body>
<!-- top开始 -->
<?php import_part("Custom.part","header"); ?><!-- main开始 -->
<div class="main">
    <div class="sidebars">
        <div class="nav_title">
            <h3>课程分类</h3>
        </div>
         <div class="main_box">
             <?php if(!empty($r)){ foreach($r['nav'] as $k=>$v){ ?>
            <div class="main_menu">
                <h3><?php echo  $v['name'];?>  <span></span></h3>
                <?php foreach($v['subClass'] as $va){?>
                <a href="<?php e_page("home","course",array('id'=> $va['id']))?>"><?php echo $va['name'] ?></a>
                <?php }?>
            </div>
            <div class="main_sub">
                <?php foreach($v['subClass'] as $value){ ?>
                <dl>
                    <dt><?php echo $value['name'] ?></dt>
                    <dd>
                        <?php foreach($value['detail_course'] as $v2){ ?>
                        <a href="<?php e_page("organization","listen",array('id'=>$v2['course_id']))?>"><?php echo $v2['course_name'] ?></a>
                        <?php }?>
                    </dd>
                </dl>
                <?php }?>
            </div>
             <?php }}else {echo null;}?>
        </div>
    </div>
    <!-- main_right开始 -->
<div class="main_right">
    <!-- 搜索框开始制作 -->
    <div class="main_right_search">
        <form class="search-form" action="<?php e_page("home","listByType") ?>" target="_blank">
            <select class="search_choose" name="type">
                <option value=1>课程</option>
                <option value=2>机构</option>
            </select>
            <input type="text" class="search-text" name="text"/>
            <input type="submit" class="search-button" value="搜索"/>
        </form>
    </div>
        <!-- 滚动图片开始制作 -->

    <div id="container">
        <div id="list" style="left: -690px;">
            <?php foreach ($r['rollpic'] as $v) {?>
                <img src="<?php echo $v['url']?>" alt=""/>
            <?php }?>
        </div>

        <div id="buttons">
            <span index="1" class="on"></span>
            <span index="2"></span>
            <span index="3"></span>
            <span index="4"></span>
            <span index="5"></span>
        </div>
        <a href="javascript:;" id="prev" class="arrow">&lt;</a>
        <a href="javascript:;" id="next" class="arrow">&gt;</a>
    </div>



        <!-- 滚动新闻开始制作 -->
        <div class="main_right_news">
            <span><a href="<?php e_page("home","rollnews",array('id'=>1))?>"><?php if(!empty($r['rollnews'])) echo $r['rollnews'][0]['rollnews_name']?></a></span>
        </div>
        <!-- 四大模块开始制作 -->
        <div class="main_right_block">
            <a href="<?php e_page("home","index");?>"><img  src="images/block-1.jpg"></a>
            <a href="<?php e_page("home","index");?>"><img  src="images/block-2.jpg"></a>
            <a href="<?php e_page("home","index");?>"><img  src="images/block-3.jpg"></a>
            <a href="<?php e_page("home","index");?>"><img  src="images/block-4.jpg"></a>
        </div>
    </div>
</div>
<!-- phone开始 -->
<!-- <div class="phone">
    <div class="phone_content">
        <p>400-234-234<span>（拨打号码可与辅导机构联系）</span></p>
    </div>
</div> -->
<!-- footer开始 -->
<?php import_part("Custom.part","footer"); ?>
</body>
</html>