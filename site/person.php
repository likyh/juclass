<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <base href="<?php echo $s['siteRoot'];?>">
    <link rel="stylesheet" type="text/css" href="styles/reset.css">
    <link rel="stylesheet" type="text/css" href="styles/person.css">
    <link rel="stylesheet" type="text/css" href="styles/main.css">
    <script type="text/javascript" src="../js/index.js"></script> 
    <title>个人主页</title>
</head>
<body>
    <!-- top开始 -->
    <?php import_part("Custom.part","header"); ?>
<!-- 开始写个人中心 -->
    <div class="person">
        <div class="person_tx"><img src="images/touxiang.jpg"></div>
        <div class="person_info">

            <?php if($r['r']==true){ ?>

            <h3>Hi:<?php echo $r['info']['phone'] ?></h3>
            <h4>欢迎来到您的个人中心</h4>
            <?php }else{ echo '请先登录';}?>
        </div>
    </div>
    <div class="my_info">
        <div class="info_list">
            <ul>
                <li><a  onclick="person(1)">我的消息</a></li>
                <li><a  onclick="person(2)">我的课表</a></li>
                <li><a  onclick="person(3)" style="border-right: 0px;">我的评价</a></li>
            </ul>
        </div>
        <div class="info_content">
            
            <?php if($r['r']==true){ ?>
            <div id="person1">111</div>

            <div id="person2" style="display:none;">
                <img src="<?php echo $r['course']['schedule_url']?>">
            </div>

            <div id="person3" style="display:none;">
                <img src="<?php echo $r['course']['image_url']?>">
                <div class="comment">

                    <?php if($r['comment']['show']==true){ ?>
                        <p>我的评价</p>
                        <p><?php echo $r['comment']['content']['content']?></p>
                    
                    <?php }else{ ?>
                
                    

                    <p><a href="#">请对本课进行评价</a><span>（建议您课程结束之后再进行评价）</span></p>
                    
                    <form method="post" action="<?php e_action('commentSubmit'); ?>">
                    <table>
                        <input type="hidden" value="<?php echo $r['info']['user_id']?>" name="user_id">
                        <input type="hidden" value="<?php echo $r['course']['course_id']?>" name="course_id">
                        <tr><td><textarea name="content">请填写评价内容</textarea></td></tr>
                        <tr><td><input type="submit" value="提交" name="send" class="submit"></td></tr>
                    </table>
                    </form> 
                <?php }?>
                </div>
            </div>
            <?php }else{ echo '尚未登录,无法查看个人中心';}?>

        </div>
    </div>   
<!-- footer开始 -->
<?php import_part("Custom.part","footer"); ?>
</body>
</html>
