<div id="data">
    <table id="dataTable" >
        <thead>
        <tr>
            <th>id</th>
            <th>手机号</th>
            <th>密码</th>
            <th>注册时间</th>
            <!-- <th>修改/删除</th> -->
        </tr>
        </thead>
        <tfoot>
        </tfoot>
        <tbody>
        <?php foreach($result['list'] as $k=>$v){?>
        <tr>
            <td><?php echo $v['user_id'] ?></td>
            <td><?php echo $v['phone'] ?></td>
            <td><?php echo $v['password'] ?></td>
            <td><?php echo $v['regTime'] ?></td>
            <!-- <th><a href="<?php e_page("user", "modify",array('id'=>$v['id'])); ?>">修改</a> /
                <a onclick="return makeSureDelete();" href="<?php e_page("user", "delete",array('id'=>$v['id'])); ?>">删除</a> </th> -->
        </tr>
        <?php }?>
        </tbody>
    </table>
    <div id="dataPage">
        <ul>
            <li>共<span><?php echo (int)$result['pageNum']; ?></span>页/<span><?php echo $result['total']; ?></span>条记录</li>
            <?php if(($result['page']-1)>0){?>
            <li ><a href="<?php e_page("user","registerUser",array('page_id'=>$result['page']-1)) ?>">上一页</a></li>
            <?php }?>
                <?php for($i=1;$i<=$result['pageNum'];$i++){?>
                <li class="page"><a href="<?php e_page("user","registerUser",array('page_id'=>$i)) ?>"><?php if($result['page']==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></li>
                <?php } ?>
                <?php if(($result['page']+1)<$result['pageNum']){?>
            <li ><a href="<?php e_page("user","registerUser",array('page_id'=>$result['page']+1)) ?>">下一页</a></li>
            <?php } ?>
        </ul>
    </div>
</div>