
<table id="dataTable">
    <thead>
   <tr>
        <th>机构名称</th>
        <th>机构电话</th>
        <th>管理</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>机构名称</th>
        <th>机构电话</th>
        <th>管理</th>
    </tr>
    </tfoot>
    <tbody>
    <?php foreach($result['list'] as $v){?>
    <tr>
        <th><?php echo $v['name']?></th>
        <th><?php echo $v['tele']?></th>
        <td><a href="<?php e_page("organization","control",array('id'=> $v['organization_id']))?>">修改</a> <a onclick="return confirm('你真的要删除吗？') ? true : false" href="<?php e_page("organization", "delete",array('id'=>$v['organization_id'])); ?>">删除</a> </td>

    </tr>
    <?php }?>
    </tbody>
</table>
<div id="dataPage">
    <ul>
        <li>共<span><?php echo (int)$result['pageNum']; ?></span>页/<span><?php echo $result['total']; ?></span>条记录</li>
        <?php if(($result['page']-1)>0){?>
        <li ><a href="<?php e_page("organization","modify",array('page_id'=>$result['page']-1)) ?>">上一页</a></li>
        <?php }?>
            <?php for($i=1;$i<=$result['pageNum'];$i++){?>
            <li class="page"><a href="<?php e_page("organization","modify",array('page_id'=>$i)) ?>"><?php if($result['page']==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></li>
            <?php } ?>
            <?php if(($result['page']+1)<$result['pageNum']){?>
        <li ><a href="<?php e_page("organization","modify",array('page_id'=>$result['page']+1)) ?>">下一页</a></li>
        <?php } ?>
    </ul>
</div>