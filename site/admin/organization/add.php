<form action="<?php e_page("organization","add_submit")?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>机构信息</legend>
        <label for="name">培训机构</label>
        <input type="text" name="name" id="name"   placeholder="请输入培训机构名字"/>
        <br/>
        <label for="organization_user">登陆账号</label>
        <input type="text" name="organization_user" id="organization_user"   placeholder="请输入机构登陆账号"/>
        <br/>
        <label for="organization_pass">登录密码</label>
        <input type="text" name="organization_pass" id="organization_pass"   placeholder="请输入机构登陆密码"/>
        <br/>
        <label for="tele">联系方式</label>
        <input type="text" name="tele" id="tele"   placeholder="请输入机构联系方式"/>
        <br/>
        <label for="qq">QQ</label>
        <input type="text" name="qq" id="qq"   placeholder="请输入机构QQ号"/>
        <br/>
        <label for="webpage">培训机构主页</label>
        <input type="text" name="webpage" id="webpage"   placeholder="请输入培训机构官方网站网址"/>
         <br/>
        <label for="address">培训机构地址</label>
        <input type="text" name="address" id="address"   placeholder="请输入培训机构地址"/>
    </fieldset>
    <fieldset>
        <label for="image_url">图片地址</label>
        <input type="file" name="picurl" id="image_url" />
    </fieldset>
    <fieldset>
        <label for="characteristic">机构特色</label>
        <textarea id="i13" name="characteristic" placeholder="请输入培训机构特色，不超过二十字"></textarea>
    </fieldset>
    <fieldset>
        <legend>请选择辅导机构类型</legend>
        <label for="yy">语言</label>
        <input type="radio" name="classify_id" id="yy" value="1" />
        <label for="zgz">资格证</label>
        <input type="radio" name="classify_id" id="zgz" value="2"/>
        <label for="ky">考研</label>
        <input type="radio" name="classify_id" id="ky" value="4"/>
        <label for="gwy">公务员</label>
        <input type="radio" name="classify_id" id="gwy" value="5"/>
        <label for="yn">艺能</label>
        <input type="radio" name="classify_id" id="yn" value="6"/>
    </fieldset>
    <input type="submit"/>
</form>


