<form action="<?php e_page("organization","change")?>" method="post"  enctype="multipart/form-data">

    <fieldset>
        <legend>机构信息</legend>
        <label for="i1">培训机构</label>
        <input type="hidden" name="id" value="<?php echo $result['organization_id']?>">
        <input type="text" name="name" id="i1" value="<?php echo $result['name']?>"  placeholder="请输入培训机构名字"/>
        <br/>
        <label for="organization_user">登陆账号</label>
        <input type="text" name="organization_user" id="organization_user"  value="<?php echo $result['organization_user']?>"  placeholder="请输入机构登陆账号"/>
        <br/>
        <label for="organization_pass">登录密码</label>
        <input type="text" name="organization_pass" id="organization_pass"  value="<?php echo $result['organization_pass']?>"  placeholder="请输入机构登陆密码"/>
        <br/>
        <label for="i1">联系方式</label>
        <input type="text" name="tele" id="i1" value="<?php echo $result['tele']?>"  placeholder="请输入机构联系方式"/>
        <br/>
        <label for="i1">QQ</label>
        <input type="text" name="qq" id="i1" value="<?php echo $result['qq']?>"  placeholder="请输入机构QQ号"/>
        <br/>
        <label for="i1">培训机构主页</label>
        <input type="text" name="webpage" id="i1" value="<?php echo $result['webpage']?>"  placeholder="请输入培训机构官方网站网址"/>
         <br/>
        <label for="i1">培训机构地址</label>
        <input type="text" name="address" id="i1" value="<?php echo $result['address']?>"  placeholder="请输入培训机构地址"/>
    </fieldset>
    <fieldset>
        <label for="i5">图片地址</label>
        <?php if(!empty($result['image_url'])) echo "已上传，重复上传将替换";else echo "未上传"; ?>
        <input type="file" name="picurl" id="i5"/>
    </fieldset>
    <fieldset>
        <label for="i13">机构特色</label>
        <textarea id="i13" name="characteristic" placeholder="请输入培训机构特色，不超过二十字"><?php echo $result['characteristic']?></textarea>
    </fieldset>
    <input type="submit"/>
</form>