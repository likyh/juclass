<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <base href="<?php echo $s['siteRoot'];?>">
    <link rel="stylesheet" type="text/css" href="styles/reset.css">
    <link rel="stylesheet" type="text/css" href="styles/listen.css">
    <link rel="stylesheet" type="text/css" href="styles/main.css">  
    <script type="text/javascript" src="js/jquery.js" ></script> 
    <script type="text/javascript" src="js/index.js"></script> 
    <title>课程信息</title>
    <link href="videojs/video-js.css" rel="stylesheet" type="text/css">
    <script src="videojs/video.js"></script>
    <script>videojs.options.flash.swf = "videojs/video-js.swf";</script>
</head>
<body>
    <!-- top开始 -->
<?php import_part("Custom.part","header"); ?><!-- main开始 -->
<!-- 显示用户浏览位置开始 -->
<div class="bread_nav">
    <a href="<?php e_page("home","index");?>">首页</a><span>&gt;&gt;</span>课程信息
</div>
<!-- 开始写课程详情，能点击视频那一页 -->
<!-- 开始写上半部分 -->
    <div class="course_all">
        <div class="course_video">


            <div id="jwplayer" style="background: transparent !important; margin:0 auto; width:680px; height:440px; overflow:hidden;">
                <div id="player">
                    <video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="680" height="440"
                      poster="http://video-js.zencoder.com/oceans-clip.png"
                      data-setup="{}">
                        <source src="aaa.mp4" type='video/mp4' />
                        <source src="http://video-js.zencoder.com/oceans-clip.webm" type='video/webm' />
                        <source src="http://video-js.zencoder.com/oceans-clip.ogv" type='video/ogg' />
                        <track kind="captions" src="demo.captions.vtt" srclang="zn" label="English"></track><!-- Tracks need an ending tag thanks to IE9 -->
                        <track kind="subtitles" src="demo.captions.vtt" srclang="zn" label="English"></track><!-- Tracks need an ending tag thanks to IE9 -->
                        <p class="vjs-no-js">对不起，您的浏览器不支持视频播放，如果您是360浏览器，请切换到极速模式</p>
                    </video>
                </div>
            </div>
        </div>
        <div class="course_p">
            <h3><a href="<?php e_page("order","confirm",array('course_id'=>$r['course_teacher']['course_id'])) ?>">￥<?php echo $r['course_teacher']['current_price'];?>  报名</a></h3>
            <p><a href="tencent://message/?uin=<?php echo $r['course_teacher']['qq']?>&Site=会&Menu=yes">点击咨询：qq交谈</a></p>

            <p>报名截止日期：<?php echo $r['course_teacher']['end_time']?></p>
            <p>上课时间：<?php echo $r['course_teacher']['lession_time']?></p>
            <p>课时：<?php echo $r['course_teacher']['class_hour']?></p>
            <p>上课地点：<?php echo $r['course_teacher']['course_adress']?></p>
            <p>评价条数：<?php echo $r['comment_num'];?></p>
            <p></p>
        </div>
    </div>
    <!-- 开始写下半部分 -->
    <div class="tabs">
        <ul>
            <li><a onclick="info_show(1)">教师介绍</a></li>
            <li><a onclick="info_show(2)">课程资料</a></li>
            <li><a onclick="info_show(3)">课表安排</a></li>
            <li><a onclick="info_show(4)">本课评价</a></li>
            <a href=<?php $a=$r['course_teacher']['organization_id']; e_page("organization","organ_course","id=$a"); ?> id="or_a">前往机构</a>
        </ul>
    </div>
    <div class="course_info">
                <div id="info1">
                    <div class="teacher_img"><img src="<?php echo $r['course_teacher']['teacher_img']?>"></div>
                    <div class="teacher_info">
                        <p>姓名：<span><?php echo $r['course_teacher']['teacher_name']?></span></p>    
                        <p>教龄:<span><?php echo $r['course_teacher']['teach_year']?></span></p>
                        <p>联系方式：<span><?php echo $r['course_teacher']['phone']?></span></p>
                        <p>邮箱：<span><?php echo $r['course_teacher']['email']?></span></p>
                        <p>教师特色：<span><?php echo $r['course_teacher']['characteristc']?></span></p>
                        <p>教师简介：<span><?php echo $r['course_teacher']['intro']?></span></p>
                    </div>
              </div> 
              <div id="info2"  style="display:none;">
                    <h3>资料下载</h3>
                    <ul>
                        <?php foreach($r['data'] as $value){?>
                        <li><span><?php echo $value['data_name']?></span><a href="<?php echo $value['data_url']?>">下载</a></li>
                        <?php }?>
                    </ul>
              </div>
              <div id="info3" style="display:none;">
                    <h3>课表详情</h3>
                    <img src="<?php echo $r['course_teacher']['schedule_url']?>">
              </div>
              <div id="info4" style="display:none;">
                    <h3>课程评价</h3>
                    <ul>
                    <?php foreach ($r['comment'] as $value){?>
                        <li>
                            <div class="com_phone"><?php echo $value['phone']?></div>
                            <div class="com_time"><?php echo $value['comment_time']?></div>
                            <div class="com_content"><?php echo $value['content']?></div>
                        </li>
                    <?php }?>
                    </ul>
              </div>
    </div>

<!-- footer开始 -->
<?php import_part("Custom.part","footer"); ?>
</body>
</html>
