$(function(){
    $('form :input').blur(function(){
         var $parent = $(this).parent();
         $parent.find(".formtips").remove();
         if( $(this).is("input[name='phone']") ){
                if( this.value=="" || ( this.value!="" && !/^1[3|5|8][0-9]{9}$/.test(this.value))){
                    var errorMsg = '<img src="images/false.png" style="width:30px;height:30px;"/>';
                    $parent.append('<span class="formtips onError">'+errorMsg+'</span>');
                }else{
                    var okMsg = '<img src="images/true.png" style="width:30px;height:30px;"/>';
                    $parent.append('<span class="formtips onSuccess">'+okMsg+'</span>');
                }
         }
         if( $(this).is("input[name='password']") ){
            if( this.value=="" || ( this.value.length<6) ){
                  var errorMsg = '<img src="images/false.png" style="width:30px;height:30px;"/>';
                  $parent.append('<span class="formtips onError">'+errorMsg+'</span>');
            }else{
                  var okMsg = '<img src="images/true.png" style="width:30px;height:30px;"/>';
                  $parent.append('<span class="formtips onSuccess">'+okMsg+'</span>');
            }
         }
    }).keyup(function(){
       $(this).triggerHandler("blur");
    }).focus(function(){
         $(this).triggerHandler("blur");
    });//end blur

     $("input[type='submit']").click(function(){
            $("form :input.required").trigger('blur');
            var numError = $('form .onError').length;
            if(numError){
                alert("请认真填写信息");
                return false;
            } 
            
     });
})