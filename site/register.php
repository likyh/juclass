<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <base href="<?php echo $s['siteRoot'];?>">
    <script src="js/jquery.js"></script>
    <script src="js/register.js"></script>
    <link rel="stylesheet" type="text/css" href="styles/reset.css">
    <link rel="stylesheet" type="text/css" href="styles/zc.css">
    <link rel="stylesheet" type="text/css" href="styles/main.css">
    <title>注册页面</title>
    
</head>
<body>
    <!-- top开始 -->
  <?php import_part("Custom.part","header"); ?>
<!-- 开始写注册表单 -->
    <div class="register">
        <img src="images/logo.png">
        <p>欢迎注册成为聚课网的小鲜肉，在这里您可以拿到最优惠的辅导机构报名咨询。更多精彩请关注聚课社区，我们在此等着您哦!</p>
        <form method="post" action="<?php e_action('registerSubmit'); ?>">
            <ul>
                <li><label>请输入手机号码</label><input  name="phone" type="text" class="required"  placeholder="手机号码">&nbsp;&nbsp;</li>
                <li><label>请 输 入 密 码</label><input name="password" type="password" class="required"  placeholder="密码">&nbsp;&nbsp;</li>
                <li><label>请再输一次</label><input name="password2" type="password" class="required"  placeholder="请再输一次">&nbsp;&nbsp;</li>
            </ul>
                <input type="checkbox" name="read"  checked disabled="disabled"><span>已认真阅读并接受<a href="<?php e_page("home","copyright",array('id'=> 3))?>">《聚课网用户协议》</a></span><br/>
                <input type="submit" name="submit"  value="创建聚课账户">
        </form>
    </div>
<!-- footer开始 -->
<?php import_part("Custom.part","footer"); ?>
</body>
</html>
