<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <base href="<?php echo $s['siteRoot'];?>">
    <link rel="stylesheet" type="text/css" href="styles/reset.css">
    <link rel="stylesheet" type="text/css" href="styles/kcxx.css">
    <link rel="stylesheet" type="text/css" href="styles/main.css">
    <title>课程信息</title>
</head>
<body>
    <!-- top开始 -->
    <?php import_part("Custom.part","header"); ?>
</div>
<!-- 显示用户浏览位置开始 -->
<div class="wrap">
<div class="bread_nav">
    <a href="<?php e_page("home","index");?>"> 首页</a><span>&gt;&gt;</span>课程列表
</div>
<!-- 显示所有课程的页面开始 -->
<div class="main_course">
    <!-- 开始写单个课程的简介 -->
    <?php foreach($r['list'] as $v){ ?>
    <div class="course">
        <div class="course_img"><a href="<?php $a=$v['course_id']; e_page("organization","listen","id=$a"); ?>"><img src="<?php echo $v['image_url'] ?>"></a></div>
        <div class="list_course">
           <h3><a href="<?php $a=$v['course_id']; e_page("organization","listen","id=$a"); ?>">课程：<?php echo $v['course_name'] ?></a></h3>
            <p>价格：<span style="font-size:24px; color:#e61c62;font-weight: bold; "><?php echo $v['current_price'] ?></span>元<span style="margin-left: 30px">原价：<del><?php echo $v['original_price'] ?></del>元</span></p>
            <p>教师：<?php echo $v['teacher_name'] ?></p>
            <p class="border">教师特色：<span><?php echo $v['characteristc'] ?></span></p>
        </div>   
        <div class="list_button">
            <input  style="margin-top: 22px" type="image" src="images/stxq.jpg" onclick="window.location.href='<?php $a=$v['course_id']; e_page("organization","listen","id=$a"); ?>'">
            <input  style="margin:22px 0 0 0" type="image" src="images/ljbm.jpg" onclick="window.location.href='<?php $a=$v['course_id']; e_page("order","confirm",array('course_id'=>$a)); ?>'">
        </div>
    </div>
    <?php }?>
<!--    <div class="course backcolor">-->
<!--    </div>-->

</div>
<!-- 开始写分页 -->
<div class="page">
    共<span><?php echo (int)$result['pageNum']; ?></span>页/<span><?php echo $result['total']; ?></span>条记录
    <?php if(($result['page']-1)>0){?>
    <span><a href="<?php e_page("home","course",array('page_id'=>$result['page']-1,'id'=>$result['id'])) ?>">上一页</a></span>
    <?php }?>
        <?php for($i=1;$i<=$result['pageNum'];$i++){?>
        <span><a href="<?php e_page("home","course",array('page_id'=>$i,'id'=>$result['id'])) ?>"><?php if($result['page']==$i) echo '<strong>'.$i.'</strong>';else echo $i; ?></a></span>
        <?php } ?>
        <?php if(($result['page']+1)<$result['pageNum']){?>
    <span><a href="<?php e_page("home","course",array('page_id'=>$result['page']+1,'id'=>$result['id'])) ?>">下一页</a></span>
    <?php } ?>
</div>
</div>
</div>
<!-- footer开始 -->
<?php import_part("Custom.part","footer"); ?>
</body>
</html>
