
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<?php echo $s['siteRoot'];?>">
<link rel="stylesheet" type="text/css" href="styles/reset.css">
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/pay.css">
<script type="text/javascript" src="js/rollpic.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<title>聚课网</title>
</head>

<body>
<!-- top开始 -->
<?php //import_part("Custom.part","header"); ?><!-- main开始 -->
<div class="pay">
    <div class="pay_top">
        <a href="<?php e_page("home","index");?>"><img src="images/logo.png"></a><span>收银台</span>
    </div>

    <span class="pay_check">确认订单消息</span>
    <form method="post" action="<?php e_page("order","create",array('course_id'=>$r['confirmInfo']['course_id'])); ?>">
            <table class="pay_table">
                <tr style="background:#2a8ebf;color:white;">
                    <td>订单编号</td>
                    <td>机构名称</td>
                    <td>课程名称</td>
                    <td>金额</td></tr>
                <tr>
                    <td>[提交后生成]</td>
                    <td><?php echo $r['confirmInfo']['organization_name'] ?></td>
                    <td><?php echo $r['confirmInfo']['course_name'] ?></td>
                    <td><?php echo $r['confirmInfo']['current_price'] ?></td>
                </tr>
            </table>

        <p><?php echo $r['userInfo']['phone'] ?>用户, 您需支付金额：<span>￥<?php echo $r['confirmInfo']['current_price'] ?></span></p>
        <input type="submit" value="提交订单">
        <input type="button" value="我要线下报名" style="background:orange;color:white" onclick="window.location.href='<?php  $a=$r['confirmInfo']['organization_id']; $b=$r['confirmInfo']['course_id'];   $c=$r['userInfo']['user_id'];  e_page("order","off_create",array('organization_id'=>$a,'course_id'=>$b,'user_id'=>$c))?>'">
    </form>
</div>



<!-- phone开始 -->
<!-- <div class="phone">
    <div class="phone_content">
        <p>400-234-234<span>（拨打号码可与辅导机构联系）</span></p>
    </div>
</div> -->
<!-- footer开始 -->
<?php import_part("Custom.part","footer"); ?>
</body>
</html>