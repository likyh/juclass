<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <base href="<?php echo $s['siteRoot'];?>">
    <script src="js/jquery.js"></script>
    <script src="js/login.js"></script>
    <link rel="stylesheet" type="text/css" href="styles/reset.css">
    <link rel="stylesheet" type="text/css" href="styles/zc.css">
    <link rel="stylesheet" type="text/css" href="styles/main.css">
    <title>登录</title>
    
</head>
<body>
    <!-- top开始 -->
    <?php import_part("Custom.part","header"); ?>
    <!-- main开始 -->

    <!-- 开始写登录表单 -->
    <div class="register">
        <img src="images/logo.png">
         <form method="post" action="<?php e_action('loginSubmit'); ?>">
            <ul>
                <li><label>请输入手机号码</label><input name="phone" class="required" type="text" value="<?php if(isset($_COOKIE["phone"])){ echo $_COOKIE['phone'];} ?>" placeholder="手机号码">&nbsp;&nbsp;</li>
                <li><label>请 输 入 密 码</label><input name="password" class="required" type="password" placeholder="密码">&nbsp;&nbsp;</li>
                <div class="left"><input type="checkbox" name="remember"><label>记住我</label><br/ ></div>
                <input type="submit" name="submit" value="登录">
            </ul>
        </form>
    </div>
<!-- footer开始 -->
<?php import_part("Custom.part","footer"); ?>
</body>
</html>