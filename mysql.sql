2014.12.1
1.ALTER TABLE `jc_order` CHANGE `couse_id` `course_id` INT( 255 ) NOT NULL COMMENT '课程ID'

2.INSERT INTO `new_juclass`.`jc_order` (
`id` ,
`user_id` ,
`course_id` ,
`progress`
)
VALUES (
NULL , '2', '2', '付款'
);


ALTER TABLE `jc_organization` CHANGE `id` `organization_id` INT( 255 ) NOT NULL AUTO_INCREMENT COMMENT 'id字段'



ALTER TABLE `jc_user` CHANGE `id` `user_id` INT( 255 ) NOT NULL AUTO_INCREMENT 


ALTER TABLE `jc_order` ADD `order_time` DATE NOT NULL COMMENT '报名时间（订单时间）'



ALTER TABLE `jc_user` CHANGE `name` `user_name` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名'



2014.12.2
ALTER TABLE `jc_order` CHANGE `id` `order_id` INT( 255 ) NOT NULL AUTO_INCREMENT 

ALTER TABLE `jc_order` CHANGE `progress` `order_progress` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单完成情况'

