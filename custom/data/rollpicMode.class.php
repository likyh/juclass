<?php
class rollpicMode extends Data{
	protected function __construct() {
        parent::__construct();
    }
    public function rollpicList($rows,$offset){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select * from jc_rollpic order by `id` desc limit $offset,$rows";
        $r=$this->db->getAll($sql);
        $total=$this->db->getValue("select count(*) from jc_rollpic ");
        return array($r,$total);
    }
    public function delete($id){
    	$id=(int)$id;
    	return $this->db->delete('jc_rollpic',$id);
    }
}
?>