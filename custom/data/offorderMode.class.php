<?php

class offorderMode extends Data{
    
    protected function __construct() {
    parent::__construct();
   }

    public function addOrder($organization_id,$course_id,$user_id){
        $sql="INSERT INTO
                                    jc_offorder
                                        (organization_id,
                                        course_id,
                                        user_id,
                                        date)
                        VALUES(
                                        '$organization_id',
                                        '$course_id',
                                        '$user_id',
                                        NOW()
                                    )";
        return $this->db->sqlExec($sql)==1;
    }

    public function getOrderList($rows,$offset,$organization_id){
        $sql="SELECT
                            r.id,
                            r.organization_id,
                            r.user_id,
                            r.course_id,
                            r.date,
                            r.offorder_progress,
                            n.name,
                            e.course_name,
                            s.phone
                FROM jc_offorder r
                LEFT JOIN jc_organization n
                ON r.organization_id=n.organization_id 
                LEFT JOIN jc_course e 
                ON  r.course_id= e.course_id
                LEFT JOIN jc_user s
                ON r.user_id=s.user_id
                WHERE r.organization_id='$organization_id'
        LIMIT $offset,$rows";
        $r=$this->db->getAll($sql);
        $total=$this->db->getValue("SELECT FOUND_ROWS()");
        return array($r,$total);
    }

    public function offorder_control($id){
        $sql="SELECT
                            id,
                            offorder_progress
                    FROM 
                            jc_offorder
                WHERE
                            id='$id'";
        $r=$this->db->getOne($sql);
        return $r;
    }

    public function offorder_change($id,$order_progress){
        $id=(int)$id;
        $order_progress=$this->db->quote($order_progress);
        $sql="UPDATE
                            jc_offorder
                    SET
                            offorder_progress={$order_progress}
                    WHERE
                            id=$id";
        return $result=$this->db->sqlExec($sql)==1;
    }

    public function delete($id){
        $id=(int)$id;
        $sql="delete from jc_offorder where id=$id";
        return $this->db->sqlExec($sql)==1;
    }

    public function getAdminOrderList($rows,$offset){
        $sql="SELECT
                            r.id,
                            r.organization_id,
                            r.user_id,
                            r.course_id,
                            r.date,
                            r.offorder_progress,
                            n.name,
                            e.course_name,
                            s.phone
                FROM jc_offorder r
                LEFT JOIN jc_organization n
                ON r.organization_id=n.organization_id 
                LEFT JOIN jc_course e 
                ON  r.course_id= e.course_id
                LEFT JOIN jc_user s
                ON r.user_id=s.user_id
        LIMIT $offset,$rows";
        $r=$this->db->getAll($sql);
        $total=$this->db->getValue("SELECT FOUND_ROWS()");
        return array($r,$total);
    }
   
}





?>