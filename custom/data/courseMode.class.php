<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/11/23
 * Time: 20:33
 */

class courseMode extends Data{
    protected function __construct() {
        parent::__construct();
        $this->session=SimpleSession::init();
    }
    //首页课程分类显示-导航
    public function nav(){
        $class['nav']=$this->db->getAll("select * from jc_classify");
        if(!empty($class)){
            foreach($class['nav'] as &$v){
                $classify_id=(int)$v['id'];
                $sql="select `course_id`, `course_name`,`subclassify_id`, `classify_id` from `jc_course` where `classify_id`={$classify_id} limit 0,10";
                $v['brief_course']=$this->db->getAll($sql);
                $sql2="select * from `jc_subclassify` where `preClassify_id`={$classify_id}";
                $v['subClass']=$this->db->getAll($sql2);
                foreach($v['subClass'] as &$v){
                    $subclassify_id=(int)$v['id'];
                    $sql3="select `course_id`, `course_name` from `jc_course` where `subclassify_id`={$subclassify_id}";
                    $v['detail_course']=$this->db->getAll($sql3);
                }
            }
        }
        $class['rollpic']=$this->db->getAll("select * from jc_rollpic");
        $class['rollnews']=$this->db->getAll("select * from jc_rollnews");
        return $class;
    }
    //课程列表-分类
    public function getList($rows,$offset,$classify_id=null){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $classify_id=empty($classify_id)?null:(int)$classify_id;
        if(!empty($classify_id)){
            $condition=" and `subclassify_id`=$classify_id ";
        }else{
            $condition='';
        }
        $sql="select C.*,T.`characteristc`,T.`intro`,T.`teacher_name` as `teacher_name` from `jc_course` C,`jc_teacherinfo` T where T.`id`=C.`teacher_id` ".$condition." limit $offset,$rows";
        $r=$this->db->getAll($sql);
        $total=$this->db->getValue("select count(*) as `teacher_name` from `jc_course` C,`jc_teacherinfo` T where T.`id`=C.`teacher_id` ".$condition);
        return array($r,$total);
    }
    //搜索课程或机构
    public function listByType($rows,$offset,$type=1,$text=null){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $type=(int)$type;
        if($type==1){   //搜课程
            if(!empty($text)){  // 搜索文本
                $condition=' and C.`course_name` like '.$this->db->quote("%{$text}%");
            }else{
               $condition=''; 
            }
            $sql="SELECT C.*,T.`characteristc`,T.`intro`,T.`teacher_name` as `teacher_name` 
            FROM `jc_course` C,`jc_teacherinfo` T 
            WHERE T.`id`=C.`teacher_id` ".$condition." limit $offset,$rows";
            $totalSql="SELECT count(*) FROM `jc_course` C,`jc_teacherinfo` T  WHERE T.`id`=C.`teacher_id` ".$condition;
        }else{  //搜机构
            if(!empty($text)){  // 搜索文本
                $condition=' where  `name` like '.$this->db->quote("%{$text}%");
            }else{
               $condition=''; 
            }
            $sql="select * from  jc_organization ".$condition." limit $offset,$rows";
            $totalSql="select count(*) from `jc_organization`";
        }
        $list=$this->db->getAll($sql);
        $total=$this->db->getValue($totalSql);
        return array($list,$total);
    }
    public function detail($id){
        $id=(int)$id;
        $sql="select JC.*,JO.`name` as `organization_name` from `jc_course` JC,`jc_organization` JO 
            where JC.`organization_id`=JO.`organization_id`
            and JC.`course_id`=$id";
        return $this->db->getOne($sql);
    }
} 