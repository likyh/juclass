<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 2014/11/26
 * Time: 20:13
 */

class adminMode extends Data{
    protected function __construct() {
        parent::__construct();
        $this->session=SimpleSession::init();
    }
    //检查登录
    public function checkLogin(){
        return isset($_SESSION['admin']['login'])&&$_SESSION['admin']['login'];
    }
    //登录
    public function login($username,$password){
        $pass=getPassWord($username,$password);
        $username=$this->db->quote($username);
        $pass=$this->db->quote($pass);
        $sql="select `id`, `username`, `status` from `jc_admin` where `username`={$username} and `password`={$pass}";
        $re=$this->db->getOne($sql);
        if(!empty($re)){
            $_SESSION['admin']['login']=true;
            $_SESSION['admin']['info']=$re;
            return true;
        }
        return false;
    }
    //登出
    public function logout(){
        $this->session->release();
    }
    //修改密码
    public function alterPass($userId,$username,$oldPass,$newPass){
        $r=array();
        $userId=(int)$userId;
        $old=getPassWord($username,$oldPass);
        var_dump($old);
        $new=getPassWord($username,$newPass);
        $username=$this->db->quote($username);
        $sql="select 1 from `jc_admin` where `username`={$username} and `password`={$old}";
        if($this->db->getExist($sql)){
            if($old==$news){
            $r['s']=0;
            $r['m']="新密码与原密码不能相同";
            return $r;
            }else{
                $data['password']=$new;
                if($this->db->modify('jc_admin',$userId,$data)==1){
                    $r['s']=1;
                    $r['m']="修改成功";
                    return $r;
                }
                $r['s']=0;
                $r['m']="修改失败";
                return $r;
            }
        }
        $r['s']=0;
        $r['m']="原密码错误";
        return $r;
    }
    //修改信息
    public function alterInfo(){

    }
} 