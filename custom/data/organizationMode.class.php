<?php

class organizationMode extends Data{
    protected function __construct() {
        parent::__construct();
        }
    //检查登录
    public function checkLogin(){
        return isset($_SESSION['org']['login'])&&$_SESSION['org']['login'];
    }
    //登录
    public function login($username,$password){
        $pass=getPassWord($username,$password);
        $username=$this->db->quote($username);
        $pass=$this->db->quote($pass);
        $sql="select `organization_id`, `classify_id`, `name`, `tele`, `qq`, `webpage`, 
        `characteristic`, `address`, `image_url`, `organization_user` 
        from `jc_organization` where `organization_user`={$username} and `organization_pass`={$pass}";
        $re=$this->db->getOne($sql);
        if(!empty($re)){
            $_SESSION['org']['login']=true;
            $_SESSION['org']['info']=$re;
            return true;
        }
        return false;
    }
    public function organizationList($rows,$offset){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select * from  jc_organization limit $offset,$rows";
        $r=$this->db->getAll($sql);
        $total=$this->db->getValue("select count(*) from `jc_organization`");
        return array($r,$total);
    }
    public function control($id){
        $id=(int)$id;
        $sql="select * from jc_organization where organization_id=$id";
        return $this->db->getOne($sql);
    }


    public function change($id,$name,$tele,$qq,$webpage,$characteristic,$address,$image_url,$organization_user,$organization_pass){
        $id=(int)$id;
        $name=$this->db->quote($name);
        $tele=$this->db->quote($tele);
        $qq=$this->db->quote($qq);
        $webpage=$this->db->quote($webpage);
        $characteristic=$this->db->quote($characteristic);
        $address=$this->db->quote($address);
        $organization_user=$this->db->quote($organization_user);
        $organization_pass=$this->db->quote($organization_pass);

        $condition='';
        if(!empty($image_url)) $condition.=",image_url={$this->db->quote($image_url)} ";
        $sql="update jc_organization set name={$name},tele={$tele},qq={$qq},webpage={$webpage},characteristic={$characteristic},address={$address},organization_user={$organization_user},organization_pass={$organization_pass} ".$condition." where organization_id=$id";
        return $this->db->sqlExec($sql)==1;
    }
    
    public function delete($id){
        $id=(int)$id;
        //联合删除，即删除辅导机构，该辅导机构的所有课程都会被删除
        $sql="delete jc_organization,jc_course from jc_organization left join jc_course on jc_organization.organization_id=jc_course.organization_id where jc_organization.organization_id=$id";
        return $this->db->sqlExec($sql)==1;
    }

    public function add($name,$organization_user,$organization_pass,$tele,$qq,$webpage,$characteristic,$address,$image_url,$classify_id){
        // $name=$this->db->quote($name);
        // $tele=$this->db->quote($tele);
        // $qq=$this->db->quote($qq);
        // $webpage=$this->db->quote($webpage);
        // $characteristic=$this->db->quote($characteristic);
        // $address=$this->db->quote($address);
        // $image_url=$this->db->quote($image_url);
        $classify_id=(int)$classify_id;
        $sql="insert into  jc_organization(classify_id,organization_user,organization_pass,name,tele,qq,webpage,characteristic,address,image_url)values('$classify_id','$organization_user','$organization_pass','$name','$tele','$qq','$webpage','$characteristic','$address','$image_url')";
        
        return $this->db->sqlExec($sql)==1;
    }

    public function organ_order($rows,$offset,$organization_id){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $organization_id=(int)$organization_id;
        $sql="select SQL_CALC_FOUND_ROWS * from jc_order inner join jc_course on jc_order.course_id=jc_course.course_id inner join jc_user on jc_order.user_id=jc_user.user_id  where jc_course.organization_id=$organization_id limit $offset,$rows ";
        $r=$this->db->getAll($sql);
        $total=$this->db->getValue("SELECT FOUND_ROWS()");
        // $total=$this->db->getValue("select count(*) from jc_order inner join jc_course on jc_order.course_id=jc_course.course_id inner join jc_user on jc_order.user_id=jc_user.user_id  where jc_course.organization_id=$organization_id");
        return array($r,$total);
    }

    public function order_control($id){
        $id=(int)$id;
        $sql="select * from jc_order inner join jc_course on jc_order.course_id=jc_course.course_id inner join jc_user on jc_order.user_id=jc_user.user_id inner join jc_organization on jc_course.organization_id=jc_organization.organization_id where jc_order.order_id=$id";
        $r=$this->db->getOne($sql);
        return $r;  
    }

    public function order_change($id,$order_progress){
        $id=(int)$id;
        $order_progress=$this->db->quote($order_progress);
        $sql="update jc_order set order_progress={$order_progress} where order_id=$id";
        return $result=$this->db->sqlExec($sql)==1;
}

    public function order_delete($id){
        $id=(int)$id;
        $sql="delete from jc_order where order_id=$id";
        return $this->db->sqlExec($sql)==1;
    }

    public function organ_teacher($rows,$offset,$organization_id){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $organization_id=(int)$organization_id;
        $sql="select SQL_CALC_FOUND_ROWS * from  jc_teacherinfo  where jc_teacherinfo.organization_id =$organization_id limit $offset,$rows ";
        $r=$this->db->getAll($sql);
        $total=$this->db->getValue("SELECT FOUND_ROWS()");
        // $total=$this->db->getValue("select count(*) from  jc_teacherinfo  where jc_teacherinfo.organization_id =$organization_id");
        return array($r,$total);
    }

    public function teacher_control($id){
        $id=(int)$id;
        $sql="select * from jc_teacherinfo where id=$id";
        $r=$this->db->getOne($sql);
        return $r;  
    }

    public function teacher_change($id,$teacher_name,$teach_year,$email,$phone,$characteristc,$intro,$teacher_img){
        $id=(int)$id;
        $condition='';
        if(!empty($teacher_img)) $condition.=",teacher_img={$this->db->quote($teacher_img)} ";
        $sql="update  jc_teacherinfo  set teacher_name='$teacher_name',teach_year='$teach_year',email='$email',phone='$phone',characteristc='$characteristc',intro='$intro' ".$condition." where id='$id'";
         return $result=$this->db->sqlExec($sql)==1;
    }

    public function teacherAdd_after($teacher_name,$teach_year,$email,$phone,$characteristc,$intro,$teacher_img,$organization_id){
        $organization_id=(int)$organization_id;
        $data['teacher_name']=$teacher_name;
        $data['teach_year']=$teach_year;
        $data['email']=$email;
        $data['phone']=$phone;
        $data['characteristc']=$characteristc;
        $data['intro']=$intro;
        $data['teacher_img']=$teacher_img;
        $data['organization_id']=$organization_id;
        return $this->db->insert('jc_teacherinfo',$data)==1;
    }

    public function teacher_delete($id){
        $id=(int)$id;
        $sql="delete from jc_teacherinfo where id=$id";
        return $this->db->sqlExec($sql)==1;
    }

    public function organ_course($rows,$offset,$organization_id){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $organization_id=(int)$organization_id;
        $sql="select SQL_CALC_FOUND_ROWS * from jc_course inner join  jc_teacherinfo on jc_course.teacher_id=jc_teacherinfo.id where jc_course.organization_id=$organization_id limit $offset,$rows";
        $r=$this->db->getAll($sql);
        $total=$this->db->getValue("SELECT FOUND_ROWS()");
        // $total=$this->db->getValue("select count(*) from jc_course inner join  jc_teacherinfo on jc_course.teacher_id=jc_teacherinfo.id where jc_course.organization_id=$organization_id ");
        return array($r,$total);
    }

    public function course_control($course_id,$organization_id){
        $course_id=(int)$course_id;
        $organization_id=(int)$organization_id;
        $sql1="select * from jc_course where jc_course.course_id=$course_id";
        $r['course']=$this->db->getOne($sql1);
        $sql2="select * from jc_teacherinfo where organization_id=$organization_id";
        $r['teacher']=$this->db->getAll($sql2);
        $sql3="SELECT * from `jc_classify`";
        $r['classify']=$this->db->getAll($sql3);
        foreach ($r['classify'] as &$value) {
            $preId=(int)$value['id'];
            $sql="SELECT * from `jc_subclassify` where `preClassify_id`=$preId";
            $value['subclassify']=$this->db->getAll($sql);
        }
        return $r;
    }

    public function course_change($course_id,$course_num,$course_name,$current_price,$original_price,$progress,$start_time,$end_time,$lession_time,$class_hour,$course_adress,$subclassify_id,$teacher_id,$video_url,$image_url,$schedule_url){
        $course_id=(int)$course_id;
        $course_num=(int)$course_num;
        $condition='';
        $course_name=$this->db->quote($course_name);
        $current_price=$this->db->quote($current_price);
        $original_price=$this->db->quote($original_price);
        $progress=$this->db->quote($progress);
        $start_time=$this->db->quote($start_time);
        $end_time=$this->db->quote($end_time);
        $lession_time=$this->db->quote($lession_time);
        $class_hour=$this->db->quote($class_hour);
        $course_adress=$this->db->quote($course_adress);
        $subclassify_id=(int)$subclassify_id;
        $teacher_id=(int)$teacher_id;
        if(!empty($video_url)) $condition.=",video_url={$this->db->quote($video_url)} ";
        if(!empty($image_url)) $condition.=",image_url={$this->db->quote($image_url)} ";
        if(!empty($schedule_url)) $condition.=",schedule_url={$this->db->quote($schedule_url)} ";
        $sql="update  jc_course  set course_num=$course_num,course_name={$course_name},current_price={$current_price},original_price={$original_price},progress={$progress},start_time={$start_time},end_time={$end_time},lession_time={$lession_time} ,class_hour={$class_hour} ,course_adress={$course_adress},subclassify_id=$subclassify_id,teacher_id=$teacher_id ".$condition."where course_id={$course_id}";
        echo $sql;
         return $result=$this->db->sqlExec($sql)==1;
    }

    public function course_add($organization_id){
        $organization_id=(int)$organization_id;
        $sql="select * from jc_teacherinfo where jc_teacherinfo.organization_id=$organization_id";
        $r['teacher']=$this->db->getAll($sql);
        $sql3="SELECT * from `jc_classify`";
        $r['classify']=$this->db->getAll($sql3);
        foreach ($r['classify'] as &$value) {
            $preId=(int)$value['id'];
            $sql="SELECT * from `jc_subclassify` where `preClassify_id`=$preId";
            $value['subclassify']=$this->db->getAll($sql);
        }
        return $r;
    }

    public function courseAdd_after($course_num,$course_name,$current_price,$original_price,$progress,$start_time,$end_time,$lession_time,$class_hour,$course_adress,$subclassify_id,$teacher_id,$organization_id,$video_url,$image_url,$schedule_url){
        $course_num=(int)$course_num;
        $course_name=$this->db->quote($course_name);
        $current_price=$this->db->quote($current_price);
        $original_price=$this->db->quote($original_price);
        $progress=$this->db->quote($progress);
        $start_time=$this->db->quote($start_time);
        $end_time=$this->db->quote($end_time);
        $lession_time=$this->db->quote($lession_time);
        $class_hour=$this->db->quote($class_hour);
        $course_adress=$this->db->quote($course_adress);
        $subclassify_id=$this->db->quote($subclassify_id);
        $teacher_id=$this->db->quote($teacher_id);
        $organization_id=$this->db->quote($organization_id);
        $video_url=$this->db->quote($video_url);
        $image_url=$this->db->quote($image_url);
        $schedule_url=$this->db->quote($schedule_url);
        $sql="insert into jc_course 
        (course_num,course_name,current_price,original_price,progress,start_time,end_time,lession_time,class_hour,course_adress,subclassify_id,teacher_id,organization_id,video_url,image_url,schedule_url) values 
        ({$course_num},{$course_name},{$current_price},{$original_price},{$progress},{$start_time},{$end_time},{$lession_time},{$class_hour},{$course_adress},{$subclassify_id},{$teacher_id},{$organization_id},{$video_url},{$image_url},{$schedule_url})";
        return  $this->db->sqlExec($sql)==1;
    }

    public function course_delete($id){
        $id=(int)$id;
        $sql="delete from jc_course where course_id=$id";
        return $this->db->sqlExec($sql)==1;
    }

    public function data($rows,$offset,$organization_id){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select SQL_CALC_FOUND_ROWS * from jc_data inner join jc_course on jc_data.course_id=jc_course.course_id  where jc_data.organization_id=$organization_id limit $offset,$rows ";
        $organization_id=(int)$organization_id;
        $r=$this->db->getAll($sql);
        $total=$this->db->getValue("SELECT FOUND_ROWS()");
        return array($r,$total);
    }

    public function data_add($organization_id){
        $organization_id=(int)$organization_id;
        $sql="select course_id,course_name from jc_course  where jc_course.organization_id=$organization_id";
        $r=$this->db->getAll($sql);
        return $r;
    }

    public function data_delete($id){
        $id=(int)$id;
        $sql="delete from jc_data where id=$id";
        return $this->db->sqlExec($sql)==1;
    }

    public function comment($rows,$offset,$organization_id){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select SQL_CALC_FOUND_ROWS * from jc_comment inner join jc_user on jc_comment.user_id=jc_user.user_id  inner join jc_course on jc_comment.course_id=jc_course.course_id  where jc_course.organization_id=$organization_id limit $offset,$rows";
        $organization_id=(int)$organization_id;
        $r=$this->db->getAll($sql);
        $total=$this->db->getValue("SELECT FOUND_ROWS()");
        return array($r,$total);
    }
}
?>