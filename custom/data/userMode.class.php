<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/11/23
 * Time: 20:14
 */

class userMode extends Data{
    protected function __construct() {
        parent::__construct();
        $this->session=SimpleSession::init();
    }
    //搜索一条用户信息
    public function oneInfo($id){
        $id=(int)$id;
        $sql="select `id`, `phone`, `email`, `nickname`, `sex`, `regTime`, `name` from `jc_user` where `id`=$id";
        return $this->db->getOne($sql);
    }
    //登录用户信息
    public function loginInfo(){
        $result=array();
        $result['info']=$_SESSION['user']['info'];
        $user_id=$result['info']['user_id'];
        $orderSql="select course_id from jc_order where user_id='$user_id'";
        $result['order']=$this->db->getOne($orderSql);
        $course_id=$result['order']['course_id'];
        $courseSql="select * from jc_course where course_id = '$course_id'";
        $result['course']=$this->db->getOne($courseSql);
        $commentSql="select * from jc_comment where user_id = '$user_id'";
        if($this->db->getOne($commentSql)){
            $result['comment']['show']=true;
            $result['comment']['content']=$this->db->getOne($commentSql);
        }else{
            $result['comment']['show']=false;
        }
        return $result;
    }
    //登录
    public function login($phone,$password){
        $pass=getPassWord($phone,$password);
        $phone=$this->db->quote($phone);
        $pass=$this->db->quote($pass);
        $checkSql="select * from `jc_user` where `phone`={$phone} and `password`={$pass}";
        $checkRe=$this->db->getOne($checkSql);
        if(!empty($checkRe)){
            $_SESSION['user']['login'] = true;
            $_SESSION['user']['info'] = $checkRe;
            return true;
        }else{
            $_SESSION['user']['login'] = false;
            return false;
        }
    }

    //检查登录
    public function checkLogin(){
        if(isset($_SESSION['user']['login'])&&$_SESSION['user']['login']==true){
            return true;
        }
        return false;
    }
    public function getLoginInfo(){
        return $_SESSION['user']['info'];
    }
    //注册
    public function register($phone,$pw,$pw2){
        if(!empty($phone)&&!empty($pw)&&!empty($pw2)){
            if($this->checkRegister($phone)){
                $r['r']=false;
                $r['m']="该账号已注册";
            }elseif($pw!=$pw2){
                $r['r']=false;
                $r['m']="两次密码不一致";
            }else{
                $data['phone']=$phone;
                $data['password']=getPassWord($phone,$pw);
                $re=$this->db->insert('jc_user',$data);
                if($re){
                    $r['r']=true;
                    $r['m']="注册成功";
                }else{
                    $r['r']=false;
                    $r['m']="注册失败";
                }
            }
        }else{
            $r['r']=0;
            $r['m']="信息不完整";
        }
        return $r;
    }
    //检查注册否
    public function checkRegister($phone){
        $phone=$this->db->quote($phone);
        $sql="select 1 from `jc_user` where `phone`={$phone}";
        return $this->db->getExist($sql);
    }
    public function registerUserList($rows,$offset){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select `user_id`, `phone`, `email`, `nickname`, `password`, `regTime`, `user_name` from `jc_user`
              where 1 order by `regTime` desc limit $offset,$rows";
        $r=$this->db->getAll($sql);
        $totalSql="select count(*) from `jc_user`";
        $total=(int)$this->db->getValue($totalSql);
        return array($r,$total);
    }
    //检查是否是手机号
    public function checkTelFormat($tel){
        return preg_match("/1[3458]{1}\d{9}$/",$tel);
    }
} 