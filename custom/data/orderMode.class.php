<?php
    class orderMode extends Data{
        protected function __construct() {
        parent::__construct();
    }
    public function orderList($rows,$offset){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select * from jc_order inner join jc_course on jc_order.course_id=jc_course.course_id inner join jc_user on jc_order.user_id=jc_user.user_id inner join jc_organization on jc_course.organization_id=jc_organization.organization_id limit $offset,$rows";
        $r=$this->db->getAll($sql);
        $total=$this->db->getValue("select count(*) from jc_order inner join jc_course on jc_order.course_id=jc_course.course_id inner join jc_user on jc_order.user_id=jc_user.user_id inner join jc_organization on jc_course.organization_id=jc_organization.organization_id");
        return array($r,$total);

    }
    public function detail($id){
        $id=(int)$id;
        $sql="select * from `jc_order` where order_id=$id";
        return $this->db->getOne($sql);
    }
    public function control($id){
        $id=(int)$id;
        $sql="select * from jc_order inner join jc_course on jc_order.course_id=jc_course.course_id inner join jc_user on jc_order.user_id=jc_user.user_id inner join jc_organization on jc_course.organization_id=jc_organization.organization_id where jc_order.order_id=$id";
        $r=$this->db->getOne($sql);
        return $r;           
    }

    public function change($id,$order_progress){
        $id=(int)$id;
        $order_progress=$this->db->quote($order_progress);
        $sql="update jc_order set order_progress={$order_progress} where order_id=$id";
        return $result=$this->db->sqlExec($sql)==1;
    }

    public function delete($id){
        $id=(int)$id;
        $sql="delete from jc_order where order_id=$id";
        return $this->db->sqlExec($sql)==1;
    }
    //生成订单
    public function create($course_id,$user_id){
        $course_id=(int)$course_id;
        $user_id=(int)$user_id;
        $re=$this->db->sqlExec("INSERT INTO `jc_order` (`course_id`,`user_id`) values ('$course_id','$user_id')");
        if($re==1){
            $order_id=$this->db->insertId();
            return array("success",$order_id);
        }else{
            return array("订单出错");
        }
    }
}

?>