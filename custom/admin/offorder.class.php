<?php 

import("custom.data.offorderMode");
import("lib.file.Uploader");
import("Custom.Admin.adminBase");

class offorder extends adminBase{
    protected function onStart(){
        parent::onStart();
        $this->session=SimpleSession::init();
        $this->offorder=offorderMode::init();
        $this->up=Uploader::init();
        $this->cms->setPageTitle("线下报名管理");
 }
    public function offorderTask(){
        $this->cms->setActionTitle("线下报名列表");
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        list($result['list'],$result['total'])=$this->offorder->getAdminOrderList(10,$offset);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $this->cms->tableScene($result,"admin/offorder/modify.php");
    }

    public function controlTask(){
        $this->cms->setActionTitle("线下报名修改");
        $id=(int)$_GET['id'];
        $result=$this->offorder->offorder_control($id);
        $this->cms->formScene($result,"admin/offorder/control.php");
     }

     public function offorder_changeTask(){
        $id=(int)$_POST['id'];
        $offorder_progress=$_POST['offorder_progress'];
        $result[0]=$this->offorder->offorder_change($id,$offorder_progress);
        $this->cms->formScene($result,"admin/offorder/control_after.php");
     }
}


?>