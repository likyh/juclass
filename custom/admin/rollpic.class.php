<?php 

import("custom.data.rollpicMode");
import("custom.data.courseMode");
import("lib.file.Uploader");
import("Custom.Admin.adminBase");

class rollpic extends adminBase{
    protected function __construct() {
        parent::__construct();
        $this->session=SimpleSession::init();
        $this->rollpic=rollpicMode::init();
        $this->course=courseMode::init();
        $this->up=Uploader::init();
        $this->cms->setPageTitle("滚动图片管理");
        $this->cms->setControlFile("admin/rollpic/control.json");
 }
    public function listTask(){
        $this->cms->setActionTitle("滚动图片列表");
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*5;
        list($result['list'],$result['total'])=$this->rollpic->rollpicList(5,$offset);
        $total=(int)$result['total'];
        $result['pageNum']=$total%5==0?(int)$total/5:(int)$total/5+1;
        $this->cms->tableScene($result,"admin/rollpic/list.php");
    }
    public function addTask(){
        $this->cms->setActionTitle("上传滚动图片");
        $this->cms->formScene(array(),"admin/rollpic/add.php");
    }
    public function addSubmitTask(){
        $this->cms->setActionTitle("上传结果");
        list($ret,$upInfo)=$this->up->upFile("picurl");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
            $data['url']=$picurl;
            $result[0]=$this->db->insert('jc_rollpic',$data)==1;
        }
        $this->cms->formScene($result,"admin/rollpic/addSubmit.php");
    }
   public function deleteTask(){
        $id=isset($_GET['id'])?(int)$_GET['id']:null;
        $r[0]=false;
        if(!empty($id)){
            $r[0]=$this->rollpic->delete($id);
        }
        $this->cms->tableScene($r,"admin/rollpic/deleteSubmit.php");
   }
   public function controlTask(){
        $result['id']=$id=isset($_GET['id'])?(int)$_GET['id']:null;
        $result['url']=$this->db->getValue("select `url` from `jc_rollpic` where id=$id");
        $this->cms->tableScene($result,"admin/rollpic/control.php");
   }
   public function controlSubmitTask(){
        $id=isset($_GET['id'])?(int)$_GET['id']:null;
        list($ret,$upInfo)=$this->up->upFile("picurl");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            var_dump($result);
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
            $data['url']=$picurl;
            $result[0]=$this->db->update('jc_rollpic',$id,$data)==1;
        }
        $this->cms->tableScene($result,"admin/rollpic/controlSubmit.php");
   }

}


?>