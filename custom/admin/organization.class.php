<?php 

import("custom.data.organizationMode");
import("custom.data.courseMode");
import("lib.file.Uploader");
import("Custom.Admin.adminBase");

class organization extends adminBase{
    protected function onStart(){
        parent::onStart();
        $this->session=SimpleSession::init();
        $this->organization=organizationMode::init();
        $this->course=courseMode::init();
        $this->up=Uploader::init();
        $this->cms->setControlFile("admin/organization/control.json");
        $this->cms->setPageTitle("机构管理");
 }
    public function modifyTask(){
        $this->cms->setActionTitle("机构列表");
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        list($result['list'],$result['total'])=$this->organization->organizationList(10,$offset);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $this->cms->tableScene($result,"admin/organization/modify.php");
    }

    public function controlTask(){
        $this->cms->setActionTitle("机构查看");
        $id=(int)$_GET['id'];
        $result=$this->organization->control($id);
        $this->cms->formScene($result,"admin/organization/control.php");
    }
    
    public function changeTask(){
        $id=$_POST['id'];
        $name=$_POST['name'];;
        $tele=$_POST['tele'];
        $qq=$_POST['qq'];
        $webpage=$_POST['webpage'];
        $characteristic=$_POST['characteristic'];
        $address=$_POST['address'];
        $organization_user=$_POST['organization_user'];
        $organization_pass=$_POST['organization_pass'];
        list($ret,$upInfo)=$this->up->upFile("picurl");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $image_url=$picurl;
        $result[0]=$this->organization->change($id,$name,$tele,$qq,$webpage,$characteristic,$address,$image_url,$organization_user,$organization_pass);
        $this->cms->formScene($result,"admin/organization/control_after.php");
    }


    public function deleteTask(){
        $id=(int)$_GET['id'];
        $result[0]=$this->organization->delete($id);
        $this->cms->formScene($result,"admin/organization/delete_after.php");
    }

    public function addTask(){
        $this->cms->setActionTitle("添加机构");
        $this->cms->setControlFile("");
        $this->cms->formScene(array(),"admin/organization/add.php");
    }

    public function add_submitTask(){
        $name=$_POST['name'];
        $organization_user=$_POST['organization_user'];   
        $organization_pass=$_POST['organization_pass'];
        $tele=$_POST['tele'];
        $qq=$_POST['qq'];
        $webpage=$_POST['webpage'];
        $characteristic=$_POST['characteristic'];
        $address=$_POST['address'];
        $classify_id=$_POST['classify_id'];
        list($ret,$upInfo)=$this->up->upFile("picurl");

        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $image_url=$picurl;
        $result[0]=$this->organization->add($name,$organization_user,$organization_pass,$tele,$qq,$webpage,$characteristic,$address,$image_url,$classify_id);
        $this->cms->formScene($result,"admin/organization/add_after.php");
    }

    
}


?>