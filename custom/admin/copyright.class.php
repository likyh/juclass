<?php 
    
import("custom.data.copyrightMode");
import("lib.file.Uploader");
import("Custom.Admin.adminBase");

class copyright extends adminBase{
    protected function onStart(){
        parent::onStart();
        $this->cms=CmsView::init("文章管理");
        $this->session=SimpleSession::init();
        $this->up=Uploader::init();
        $this->copyright=copyrightMode::init();
        $this->cms->setPageTitle("底部版权管理");
     }
     public function modifyTask(){
        $this->cms->setActionTitle("底部信息列表");
        $rows=isset($_GET['rows'])?(int)$_GET['rows']:12;
        $offset=isset($_GET['offset'])?(int)$_GET['offset']:0;
        $result['list']=$this->copyright->copyrightList($rows,$offset);
        $this->cms->tableScene($result,"admin/copyright/copyright.php");
    }

    public function controlTask(){
        $this->cms->setActionTitle("底部信息查看");
        $id=$_GET['id'];
        $result['list']=$this->copyright->control($id);
        $this->cms->normalScene($result,"admin/copyright/control.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }

    public function changeTask(){
        $copyright_id=$_POST['copyright_id'];
        $name=$_POST['name'];
        $content=$_POST['content'];
        $result[0]=$this->copyright->change($copyright_id,$name,$content);
        $this->cms->formScene($result,"admin/copyright/control_after.php");
    }

}


?>