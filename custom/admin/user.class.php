<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/11/26
 * Time: 18:49
 */
import("custom.data.userMode");
import("custom.data.courseMode");
import("lib.file.Uploader");
import("Custom.Admin.adminBase");

class user extends adminBase{
    protected function onStart(){
        parent::onStart();
        $this->session=SimpleSession::init();
        $this->user=userMode::init();
        $this->course=courseMode::init();
        $this->up=Uploader::init();
        $this->cms->setPageTitle("用户管理");
    }
    public function registerUserTask(){
        $this->cms->setActionTitle("用户列表");
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        list($result['list'],$result['total'])=$this->user->registerUserList(10,$offset);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $this->cms->tableScene($result,"admin/user/registerUser.php");
     
    }
    public function userInfoTask(){

    }

    public function modifyTask(){
        
    }
    public function messageTask(){
        echo "hap";
    }
} 