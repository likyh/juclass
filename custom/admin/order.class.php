<?php 

import("custom.data.orderMode");
import("custom.data.courseMode");
import("lib.file.Uploader");
import("Custom.Admin.adminBase");

class order extends adminBase{
    protected function onStart(){
        parent::onStart();
        $this->session=SimpleSession::init();
        $this->order=orderMode::init();
        $this->course=courseMode::init();
        $this->up=Uploader::init();
        $this->cms->setPageTitle("报名管理");
 }
    public function modifyTask(){
        $this->cms->setActionTitle("报名列表");
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        list($result['list'],$result['total'])=$this->order->orderList(10,$offset);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $this->cms->tableScene($result,"admin/order/modify.php");
    }
    
    public function controlTask(){
        $this->cms->setActionTitle("报名查看");
        $id=$_GET['id'];
        $result=$this->order->control($id);
        $this->cms->formScene($result,"admin/order/control.php");
    }

    public function changeTask(){
        $id=$_POST['order_id'];
        $order_progress=$_POST['order_progress'];
        $result[0]=$this->order->change($id,$order_progress);
        $this->cms->formScene($result,"admin/order/control_after.php");
    }

    public function deleteTask(){
        $id=$_GET['id'];
        $result[0]=$this->order->delete($id);
        $this->cms->formScene($result,"admin/order/delete_after.php");
    }
}


?>