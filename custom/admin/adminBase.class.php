<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 2014/11/26
 * Time: 20:07
 */
import('Custom.Data.adminMode');
class adminBase extends Activity{
    /** @var CmsView */
    protected $cms;
    /** @var  adminMode */
    protected $admin;
    protected function onStart() {
        parent::onStart();
        $this->admin=adminMode::init();
        $this->cms=CmsView::init();
        $this->db=SqlDB::init();
        $this->checkLogin();
    }

    function checkLogin(){
       if(!$this->admin->checkLogin()){
           $web=WebRouter::init();
           header("Location:".$web->getPage("admin","login"));
           exit();
       }
       $this->cms->setUserName($_SESSION['admin']['info']['username']);
    }
}