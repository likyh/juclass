<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 2014/11/26
 * Time: 20:00
 */
import("custom.data.indexMode");
import("custom.data.courseMode");
import("lib.file.Uploader");
import("Custom.Admin.adminBase");
class home extends adminBase{
    protected $cms;
    protected function onStart(){
        parent::onStart();
        $this->session=SimpleSession::init();
        $this->index=indexMode::init();
        $this->cms->setPageTitle("欢迎页面");
    }
    public function indexTask(){
        $rows=isset($_GET['rows'])?(int)$_GET['rows']:12;
        $offset=isset($_GET['offset'])?(int)$_GET['offset']:0;
        $result['list']=$this->index->indexList($rows,$offset);
        $this->cms->tableScene($result,"admin/index.php");
    }
} 

?>