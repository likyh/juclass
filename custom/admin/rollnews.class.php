<?php 
    
import("custom.data.rollnewsMode");
import("lib.file.Uploader");
import("Custom.Admin.adminBase");

class rollnews extends adminBase{
    protected function onStart(){
        parent::onStart();
        $this->cms=CmsView::init("信息管理");
        $this->session=SimpleSession::init();
        $this->up=Uploader::init();
        $this->rollnews=rollnewsMode::init();
        $this->cms->setPageTitle("信息管理");
     }
     public function modifyTask(){
        $this->cms->setActionTitle("信息列表");
        $result['list']=$this->rollnews->rollnewsList();
        $this->cms->tableScene($result,"admin/rollnews/rollnews.php");
    }

    public function controlTask(){
        $this->cms->setActionTitle("信息修改");
        $result['list']=$this->rollnews->control();
        $this->cms->normalScene($result,"admin/rollnews/control.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }

    public function changeTask(){
        $rollnews_id=$_POST['id'];
        $name=$_POST['name'];
        $content=$_POST['content'];
        $result[0]=$this->rollnews->change($rollnews_id,$name,$content);
        $this->cms->formScene($result,"admin/rollnews/control_after.php");
    }

}


?>