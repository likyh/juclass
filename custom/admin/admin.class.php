<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 2014/11/26
 * Time: 20:08
 */
import("custom.admin.adminBase");
import("custom.data.adminMode");
class admin extends adminBase{
    /** @var  adminMode */
    protected $admin;
    protected function onStart(){
        // parent::onStart();
        $this->cms=CmsView::init();
        $this->admin=adminMode::init();
    }
    public function loginTask(){
        $web=WebRouter::init();
        $this->cms->loginScene($web->getAction("loginSubmit"));
    }
    public function loginSubmitTask(){
        $user=isset($_POST['user'])?$_POST['user']:null;
        $pass=isset($_POST['pass'])?$_POST['pass']:null;
        if($this->admin->login($user, $pass)){
            echo "succuss";
            $web=WebRouter::init();
            header("Location:".$web->getPage("home","index"));
            exit();
        }else{
            echo "登录失败";
        }
     }
     public function logoutTask(){
        $this->admin->logout();
        $web=WebRouter::init();
         header("Location:".$web->getPage("admin","login"));
     }
} 