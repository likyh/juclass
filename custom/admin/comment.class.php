<?php 
    
import("custom.data.commentMode");
import("custom.data.courseMode");
import("lib.file.Uploader");
import("Custom.Admin.adminBase");

class comment extends adminBase{
    protected function onStart(){
        parent::onStart();
        $this->session=SimpleSession::init();
        $this->comment=commentMode::init();
        $this->course=courseMode::init();
        $this->up=Uploader::init();
        $this->cms->setPageTitle("评价管理");
     }
     public function modifyTask(){
        $this->cms->setActionTitle("评价列表");
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        list($result['list'],$result['total'])=$this->comment->commentList(10,$offset);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $this->cms->tableScene($result,"admin/comment/modify.php");
    }
    public function controlTask(){
        $this->cms->setActionTitle("评价查看");
        $id=$_GET['id'];
        $result['list']=$this->comment->controlList($id);
        $this->cms->formScene($result,"admin/comment/control.php");
    }

    public function changeTask(){
        $id=$_POST['id'];
        $content=$_POST['content'];
        $result['list']=$this->comment->changeList($id,$content);
        $this->cms->formScene($result,"admin/comment/control_after.php");
    }

    public function deleteTask(){
        $id=$_GET['id'];
        $result['list']=$this->comment->deleteList($id);
        $this->cms->formScene($result,"admin/comment/delete_after.php");
    }

}


?>