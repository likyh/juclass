<?php
import('Custom.Data.organizationMode');
import('Custom.Data.offorderMode');
import("lib.file.Uploader");
class home extends Activity{
    /** @var CmsView */
    protected $cms;
    /** @var  adminMode */
    protected $admin;
    protected function onStart() {
        parent::onStart();
        $this->admin=organizationMode::init();
        $this->offorder=offorderMode::init();
        $this->cms=CmsView::init();
        $this->db=SqlDB::init();
        $this->up=Uploader::init();
        $this->session=SimpleSession::init();
        $this->cms->setPageTitle("欢迎页面");
        $this->cms->loadConfig(array("navFile"=>"org/nav.json"));
        $this->cms->loadConfig(array("userInfoFile"=>"org/userInfo.json"));
    }

    function checkLogin(){
       if(!$this->admin->checkLogin()){
           $web=WebRouter::init();
           header("Location:".$web->getPage("home","login"));
           exit();
       }
       $this->cms->setUserName($_SESSION['org']['info']['name']);
    }
    public function indexTask(){
        $this->checkLogin();
        $this->cms->tableScene(array(),"org/index.php");
    }
    public function loginTask(){
        $web=WebRouter::init();
        $this->cms->loginScene($web->getAction("loginSubmit"));
    }
    public function loginSubmitTask(){
        $user=isset($_POST['user'])?$_POST['user']:null;
        $pass=isset($_POST['pass'])?$_POST['pass']:null;
        if($this->admin->login($user, $pass)){
            echo "succuss";
            $web=WebRouter::init();
            header("Location:".$web->getPage("home","index"));
            exit();
        }else{
            echo "登录失败";
        }
     }
     public function logoutTask(){
         session_destroy();
         $web=WebRouter::init();
         header("Location:".$web->getPage("home","login"));
     }
     public function orderTask(){
        $this->cms->setActionTitle("线上报名列表");
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        $organization_id=$_SESSION['org']['info']['organization_id'];
        list($result['list'],$result['total'])=$this->admin->organ_order(10,$offset,$organization_id);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $this->cms->tableScene($result,"org/order/order.php");
     }
     public function order_controlTask(){
        $this->cms->setActionTitle("线上报名查看");
        $id=(int)$_GET['id'];
        $result=$this->admin->order_control($id);
        $this->cms->formScene($result,"org/order/order_control.php");
     }

     public function order_changeTask(){
        $id=(int)$_POST['order_id'];
        $order_progress=$_POST['order_progress'];
        $result[0]=$this->admin->order_change($id,$order_progress);
        $this->cms->formScene($result,"org/order/control_after.php");
     }

     public function order_deleteTask(){
        $id=(int)$_GET['id'];
        $result[0]=$this->admin->order_delete($id);
        $this->cms->formScene($result,"org/order/delete_after.php");
     }

     public function teacherTask(){
        $this->cms->setActionTitle("教师列表");
        $this->cms->setControlFile("org/teacher/control.json");
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        $organization_id=$_SESSION['org']['info']['organization_id'];
        list($result['list'],$result['total'])=$this->admin->organ_teacher(10,$offset,$organization_id);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $this->cms->tableScene($result,"org/teacher/teacher.php");
     }

     public function teacher_controlTask(){
        $this->cms->setActionTitle("教师查看");
        $id=(int)$_GET['id'];
        $result['list']=$this->admin->teacher_control($id);
        $this->cms->formScene($result,"org/teacher/teacher_control.php");
     }


     public function teacher_changeTask(){
        $id=(int)$_POST['id'];
        $teacher_name=$_POST['teacher_name'];
        $teach_year=$_POST['teach_year'];
        $email=$_POST['email'];
        $phone=$_POST['phone'];
        $characteristc=$_POST['characteristc'];
        $intro=$_POST['intro'];
        list($ret,$upInfo)=$this->up->upFile("teacher_img");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $teacher_img=$picurl;
        $result[0]=$this->admin->teacher_change($id,$teacher_name,$teach_year,$email,$phone,$characteristc,$intro,$teacher_img);
        $this->cms->formScene($result,"org/teacher/teacher_after.php");
     }

     public function teacher_addTask(){
        $this->cms->setActionTitle("添加教师");
        $this->cms->formScene(array(),"org/teacher/add.php");
     }

     public function teacherAdd_afterTask(){
        $name=$_POST['teacher_name'];
        $teach_year=$_POST['teach_year'];
        $email=$_POST['email'];
        $phone=$_POST['phone'];
        $characteristc=$_POST['characteristc'];
        $intro=$_POST['intro'];
        $organization_id=$_SESSION['org']['info']['organization_id'];  
        $organization_id=(int)$organization_id;
        list($ret,$upInfo)=$this->up->upFile("teacher_img");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $teacher_img=$picurl;    
        $result[0]=$this->admin->teacherAdd_after($name,$teach_year,$email,$phone,$characteristc,$intro,$teacher_img,$organization_id);
        $this->cms->formScene($result,"org/teacher/teacherAdd_after.php");
     }

     public function teacher_deleteTask(){
        $id=(int)$_GET['id'];
        $result[0]=$this->admin->teacher_delete($id);
        $this->cms->formScene($result,"org/teacher/delete_after.php");
     }

     public function courseTask(){
        $this->cms->setActionTitle("课程列表");
        $this->cms->setControlFile("org/course/control.json");
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        $organization_id=$_SESSION['org']['info']['organization_id'];
        list($result['list'],$result['total'])=$this->admin->organ_course(10,$offset,$organization_id);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $this->cms->tableScene($result,"org/course/course.php");
    }

    public function course_controlTask(){
        $this->cms->setActionTitle("课程查看");
        $course_id=(int)$_GET['id'];
        $organization_id=$_SESSION['org']['info']['organization_id'];
        $result=$this->admin->course_control($course_id,$organization_id);
        $this->cms->formScene($result,"org/course/course_control.php");
    }

    public function course_changeTask(){
        if(empty($_POST['course_id'])||empty($_POST['teacher_id'])||empty($_POST['subclassify_id'])){
            $result[0]=false;
            $this->cms->formScene($result,"org/course/course_after.php");
            return;
        }
        $course_id=$_POST['course_id'];
        $course_id=(int)$course_id;
        $course_num=$_POST['course_num'];
        $course_name=$_POST['course_name'];
        $current_price=$_POST['current_price'];
        $original_price=$_POST['original_price'];
        $progress=$_POST['progress'];
        $start_time=$_POST['start_time'];
        $end_time=$_POST['end_time'];
        $lession_time=$_POST['lession_time'];
        $class_hour=$_POST['class_hour'];
        $course_adress=$_POST['course_adress'];
        $subclassify_id=$_POST['subclassify_id'];
        $teacher_id=$_POST['teacher_id'];
    
        list($ret,$upInfo)=$this->up->upFile("video_url");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $video_url=$picurl;

        list($ret,$upInfo)=$this->up->upFile("image_url");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $image_url=$picurl;

        list($ret,$upInfo)=$this->up->upFile("schedule_url");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $schedule_url=$picurl;
        $result[0]=$this->admin->course_change($course_id,$course_num,$course_name,$current_price,
            $original_price,$progress,$start_time,$end_time,$lession_time,$class_hour,
            $course_adress,$subclassify_id,$teacher_id,$video_url,$image_url,$schedule_url);
        $this->cms->formScene($result,"org/course/course_after.php");
    }

    public function course_addTask(){
        $this->cms->setActionTitle("添加课程");
        $organization_id=$_SESSION['org']['info']['organization_id'];
        $organization_id=(int)$organization_id;
        $result=$this->admin->course_add($organization_id);
        $this->cms->formScene($result,"org/course/add.php");
    }

    public function courseAdd_afterTask(){
        if(empty($_POST['teacher_id'])||empty($_POST['subclassify_id'])){
            $result[0]=false;
            $this->cms->formScene($result,"org/course/courseAdd_after.php");
            return;
        }
        $course_num=$_POST['course_num'];
        $course_name=$_POST['course_name'];
        $current_price=$_POST['current_price'];
        $original_price=$_POST['original_price'];
        $progress=$_POST['progress'];
        $start_time=$_POST['start_time'];
        $end_time=$_POST['end_time'];
        $lession_time=$_POST['lession_time'];
        $class_hour=$_POST['class_hour'];
        $course_adress=$_POST['course_adress'];
        $subclassify_id=$_POST['subclassify_id'];
        $teacher_id=$_POST['teacher_id'];
        $organization_id=$_SESSION['org']['info']['organization_id'];

         list($ret,$upInfo)=$this->up->upFile("video_url");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $video_url=$picurl;

        list($ret,$upInfo)=$this->up->upFile("image_url");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $image_url=$picurl;

        list($ret,$upInfo)=$this->up->upFile("schedule_url");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $schedule_url=$picurl;

        $result[0]=$this->admin->courseAdd_after($course_num,$course_name,$current_price,$original_price,
            $progress,$start_time,$end_time,$lession_time,$class_hour,$course_adress,$subclassify_id,
            $teacher_id,$organization_id,$video_url,$image_url,$schedule_url);
        $this->cms->formScene($result,"org/course/courseAdd_after.php");
    }

    public function course_deleteTask(){
        $id=(int)$_GET['id'];
        $result[0]=$this->admin->course_delete($id);
        $this->cms->formScene($result,"org/course/delete_after.php");
    }

    public function dataTask(){
        $this->cms->setActionTitle("资料列表");
        $this->cms->setControlFile("org/data/control.json");
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        $organization_id=$_SESSION['org']['info']['organization_id'];
        list($result['list'],$result['total'])=$this->admin->data(10,$offset,$organization_id);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $this->cms->tableScene($result,"org/data/data.php");
    }

    public function data_addTask(){
        $this->cms->setActionTitle("上传资料");
        $organization_id=$_SESSION['org']['info']['organization_id'];
        $result['list']=$this->admin->data_add($organization_id);
        $this->cms->formScene($result,"org/data/add.php");
    }
    public function data_add_submitTask(){
        $courser_id=(int)$_POST['course_id'];
        $data_name=$_POST['data_name'];
        $organization_id=$_SESSION['org']['info']['organization_id'];
        list($ret,$upInfo)=$this->up->upFile("resource");
        if(strtolower($ret)!="success"){
            $result[0]=false;
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
            $data['data_url']=$picurl;
            $data['course_id']=$courser_id;
            $data['organization_id']=(int)$organization_id;
            $data['data_name']=$data_name;
            $result[0]=$this->db->insert('jc_data',$data)==1;
        }
        $this->cms->tableScene($result,"org/data/addSubmit.php");
    }

    public function data_deleteTask(){
        $id=(int)$_GET['id'];
        $result[0]=$this->admin->data_delete($id);
        $this->cms->formScene($result,"org/data/delete_after.php");
    }

    public function commentTask(){
        $this->cms->setActionTitle("评价列表");
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        $organization_id=$_SESSION['org']['info']['organization_id'];
        list($result['list'],$result['total'])=$this->admin->comment(10,$offset,$organization_id);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $this->cms->tableScene($result,"org/comment/comment.php");
    }
    public function newsTask(){
        $this->cms->setActionTitle("消息列表");
    }

    public function offorderTask(){
        $this->cms->setActionTitle("线下报名列表");
        $organization_id=$_SESSION['org']['info']['organization_id'];
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        $organization_id=$_SESSION['org']['info']['organization_id'];
        list($result['list'],$result['total'])=$this->offorder->getOrderList(10,$offset,$organization_id);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $this->cms->tableScene($result,"org/order/offorder.php");
    }

    public function offorder_controlTask(){
        $this->cms->setActionTitle("线下报名查看");
        $id=(int)$_GET['id'];
        $result=$this->offorder->offorder_control($id);
        $this->cms->formScene($result,"org/order/offorder_control.php");
     }

     public function offorder_changeTask(){
        $id=(int)$_POST['id'];
        $offorder_progress=$_POST['offorder_progress'];
        $result[0]=$this->offorder->offorder_change($id,$offorder_progress);
        $this->cms->formScene($result,"org/order/offcontrol_after.php");
     }
     public function offorder_deleteTask(){
        $id=(int)$_GET['id'];
        $result[0]=$this->offorder->delete($id);
        $this->cms->formScene($result,"org/data/delete_after.php");
    }

}