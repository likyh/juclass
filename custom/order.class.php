<?php
import("custom.data.userMode");
import("custom.data.courseMode");
import("custom.data.orderMode");
import("custom.data.offorderMode");
class order extends Activity{
    protected function __construct() {
        parent::__construct();
        $this->db=SqlDB::init();
        $this->session=SimpleSession::init();
        $this->user=userMode::init();
        $this->course=courseMode::init();
        $this->order=orderMode::init();
        $this->offorder=offorderMode::init();
    }
    //等待用户确定订单
    function confirmTask(){
        if($this->user->checkLogin()){
             if(isset($_GET['course_id'])&&!empty($_GET['course_id'])){
                $result['confirmInfo']=$this->course->detail($_GET['course_id']);
                $result['userInfo']=$this->user->getLoginInfo();
                View::displayAsHtml($result,'pay1.php'); 
            }else{
                // header("Location:".WebRouter::init()->getPage("home","index"));
                echo "订单错误";
            }   
        }else{
            header("Location:".WebRouter::init()->getPage("home","login"));
        }
    }
    //用户确认订单
    public function createTask(){
        if($this->user->checkLogin()){
            if(isset($_GET['course_id'])&&!empty($_GET['course_id'])){
                $userInfo=$this->user->getLoginInfo();
                $user_id=(int)$userInfo['user_id'];
                $result=$this->order->create($_GET['course_id'],$user_id);
                View::displayAsHtml($result,'pay2.php'); 
            }else{
                echo "订单错误";
            }
        }else{
            header("Location:".WebRouter::init()->getPage("home","login"));
        }
    }
    public function alipayTask(){
        if(isset($_POST['order_id'])&&!empty($_POST['order_id'])){
            $order=$this->order->detail($_POST['order_id']);
            $course_id=(int)$order['course_id'];
            $course=$this->course->detail($course_id);
            $result['WIDout_trade_no']=$order['order_id'];
            $result['WIDtotal_fee']=$course['current_price'];
            $result['WIDsubject']=$course['course_name'];
            $result['WIDshow_url']=WebRouter::init()->getPage("organization","listen",array("id"=>$course_id)); 
            View::displayAsHtml($result,'../alipay/alipayapi.php');
        }else{
            echo "error";
        }
    }
    function return_urlTask(){
        require_once("alipay/alipay.config.php");
        require_once("alipay/lib/alipay_notify.class.php");
        $alipayNotify = new AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyReturn();
        if($verify_result) {//验证成功
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //请在这里加上商户的业务逻辑程序代码
            
            //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
            //获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表

            //商户订单号
            $out_trade_no = $_GET['out_trade_no'];
            //支付宝交易号
            $trade_no = $_GET['trade_no'];
            //交易状态
            $trade_status = $_GET['trade_status'];
            //TODO处理订单
            $order_id=(int)$out_trade_no;
            $this->order->change($order_id,1);
            if($_GET['trade_status'] == 'TRADE_FINISHED' || $_GET['trade_status'] == 'TRADE_SUCCESS') {
                //判断该笔订单是否在商户网站中已经做过处理
                    //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                    //如果有做过处理，不执行商户的业务程序
            }
            else {
              echo "trade_status=".$_GET['trade_status'];
            }    
            $return = true;
            //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——        
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }
        else {
            //验证失败
            //如要调试，请看alipay_notify.php页面的verifyReturn函数
            $return= false;
        }
        if($return){
            View::displayAsHtml(array(),'pay3.php');
        }else{
            View::displayAsHtml(array(),'pay4.php');
        }
    }

    //线下报名
    public function off_createTask(){
        if(isset($_GET['organization_id']) && !empty($_GET['organization_id'])){
            $organization_id=$_GET['organization_id'];
            $course_id=$_GET['course_id'];
            $user_id=$_GET['user_id'];
            if($this->offorder->addOrder($organization_id,$course_id,$user_id)){
                echo '<script type="text/javascript">alert("报名成功！请等待辅导机构老师联系！");history.back();</script>';
                //header("Location:".WebRouter::init()->getPage("home","index"));
            }else{
                echo '<script type="text/javascript">alert("报名失败！请与聚课网客服联系！");history.back();</script>';
            }
        }
    }

  


}

?>