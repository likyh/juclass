<?php
import("custom.data.userMode");
import("custom.data.courseMode");
import("custom.data.indexMode");
import("custom.data.commentMode");
class home extends Activity{
    protected function __construct() {
        parent::__construct();
        $this->db=SqlDB::init();
        $this->session=SimpleSession::init();
        $this->user=userMode::init();
        $this->course=courseMode::init();
        $this->index=indexMode::init();
        $this->comment=commentMode::init();
    }
    function indexTask(){
        $result=$this->course->nav();
        View::displayAsHtml($result,'index.php');
    }
    function loginTask(){
        $r=array();
        View::displayAsHtml($r,'login.php');
    }
    function loginSubmitTask(){
        $web=WebRouter::init();
        if($this->user->checkLogin()){
            header("Location:".$web->getPage("home","person"));
        }
        elseif(isset($_POST['phone'])&&isset($_POST['password'])&&!empty($_POST['phone'])&&!empty($_POST['password'])){
                $re=$this->user->login($_POST['phone'],$_POST['password']);
                if($re){
                    if(isset($_POST['remember'])){
                        //setcookie("phone",$_POST['phone'],time()-36000);
                        setcookie("phone",$_POST['phone'],time()+36000,"/",null,false,true);
                    }
                    header("Location:".$web->getPage("home","person"));
                    $r['r']=true;
                    $r['m']="登录成功";
                }else{
                    $r['r']=false;
                    $r['m']="登录失败";
                }
            }else{
                $r['r']=false;
                $r['m']="请填写登录信息";
            }
        View::displayAsHtml($r,'person.php');

    }
    public function logoutTask(){
        session_destroy();
        $web=WebRouter::init();
        header("Location:".$web->getPage("home","login"));
    }
    function personTask(){
        if(!$this->user->checkLogin()){
            $r['r']=false;
        }else{
            $r=$this->user->loginInfo();
            $r['r']=true;
        }
        View::displayAsHtml($r,'person.php');
    }
    function courseTask(){
        $result=array();
        $rows=isset($_GET['rows'])?(int)$_GET['rows']:10;
        $offset=isset($_GET['offset'])?(int)$_GET['offset']:0;
        $classify_id=isset($_GET['id'])?(int)$_GET['id']:1;
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        list($result['list'],$result['total'])=$this->course->getList($rows,$offset,$classify_id);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        $result['id']=$classify_id;     //分类id
        View::displayAsHtml($result,'course.php');
    }
    function registerTask(){
        $r=array();
        View::displayAsHtml($r,'register.php');
    }
    function registerSubmitTask(){
        $result=array();
        if(isset($_POST['phone'])&&isset($_POST['password'])&&isset($_POST['password2'])){
        $result=$this->user->register($_POST['phone'],$_POST['password'],$_POST['password2']);
        }
        View::displayAsHtml($result,'registerSubmit.php');

    }

    function copyrightTask(){
        $result=array();
        $id=(int)$_GET['id'];
        $result=$this->index->copyright($id);
        View::displayAsHtml($result,'copyright.php');
    }

    function rollnewsTask(){
        $result=array();
        $id=(int)$_GET['id'];
        $result=$this->index->rollnews($id);
        View::displayAsHtml($result,'rollnews.php');
    }
    function listByTypeTask(){
        $text=isset($_GET['text'])?$_GET['text']:null;
        $type=isset($_GET['type'])?(int)$_GET['type']:1;
        $offset=isset($_GET['offset'])?(int)$_GET['offset']:0;
        $classify_id=isset($_GET['id'])?(int)$_GET['id']:1;
        $result['page']=$page=isset($_GET['page_id'])?(int)$_GET['page_id']:1;
        $offset=((int)$page<=1)? 0:($page-1)*10;
        list($result['list'],$result['total'])=$this->course->listByType(10,$offset,$type,$text);
        $total=(int)$result['total'];
        $result['pageNum']=$total%10==0?(int)$total/10:(int)$total/10+1;
        if($type==1){
            View::displayAsHtml($result,'courseList.php');
        }else{
            View::displayAsHtml($result,'organList.php');
        }
    }

    function commentSubmitTask(){
        $content=$_POST['content'];
        $user_id=$_POST['user_id'];
        $course_id=$_POST['course_id'];
        if($this->comment->addComment($content,$user_id,$course_id)){
            $web=WebRouter::init();
            header("Location:".$web->getPage("home","person"));
        }
    }
}
